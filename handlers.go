package main

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"

	"crypto/hmac"

	"encoding/hex"

	"database/sql"

	"strings"

	"github.com/captaincodeman/couponcode"
	"github.com/julienschmidt/httprouter"
)

var contentType = "Content-Type"
var appJSON = "application/json; charset=UTF-8"

//SanitizeParams checks whether params in the URI are alphanumeric, it should be called on top of Auth handlers
func SanitizeParams(protected httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		//sanitize params
		for _, p := range ps {
			if p.Key == "code" {
				if !isCode(p.Value) {
					//invalid uri
					http.Error(w, "400 invalid uri", http.StatusBadRequest)
					return
				}
				continue
			}
			if !isAlphanumeric(p.Value) {
				//invalid uri
				http.Error(w, "400 invalid uri", http.StatusBadRequest)
				return
			}
			//subid has to be numeric
			if p.Key == "subid" {
				if !isNumeric(p.Value) {
					//invalid uri
					http.Error(w, "400 invalid uri", http.StatusBadRequest)
					return
				}
			}
		}
		w.Header().Set("Access-Control-Allow-Origin", "*")
		protected(w, r, ps)
	})
}

//AuthApp handles authentication before passing the request to its respective handler.
//It checks the supplied hash against the application hash only.
func AuthApp(protected httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		//handle stale requests
		requestDate := r.Header.Get("When")
		if requestDate == "" {
			http.Error(w, "400 'When' header is required", http.StatusBadRequest)
			return
		}
		d, err := time.Parse(http.TimeFormat, requestDate)
		if err != nil {
			panic(err)
		}
		if time.Now().Before(d) {
			//request from the future
			http.Error(w, "400 request postdates response", http.StatusBadRequest)
			return
		}
		if diff := time.Now().Sub(d); diff > 5*time.Minute {
			//stale request
			http.Error(w, "400 stale request", http.StatusBadRequest)
			return
		}

		a, err := selectApp(ps.ByName("appname"))
		if err != nil {
			if err != sql.ErrNoRows {
				panic(err)
			}
			//replying with 401 here to avoid enumeration attacks
			fmt.Printf("%s resource not found during Auth, replying with a 401.\n", "App")
			notAuthorized(w, r)
			return
		}

		h, err := hex.DecodeString(r.Header.Get("Authorization"))
		if err != nil {
			//invalid Authorization, has to be a hex string
			http.Error(w, "400 illegal characters in Authorization header", http.StatusBadRequest)
		}

		payload, err := ioutil.ReadAll(r.Body)
		if err != nil {
			panic(err)
		}
		//creating a new reader so subsequent handlers have something to read from
		r.Body = ioutil.NopCloser(bytes.NewReader(payload))

		m := append(payload, []byte(requestDate)...)

		if authApp(a, h, m) {
			protected(w, r, ps)
		} else {
			notAuthorized(w, r)
		}
	})
}

//AuthUser handles authentication before passing the request to its respective handler.
//It checks the supplied hash against both the application hash and the user hash (in that order).
//Success on either check is sufficient.
func AuthUser(protected httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		//handle stale requests
		requestDate := r.Header.Get("When")
		if requestDate == "" {
			http.Error(w, "400 'When' header is required", http.StatusBadRequest)
			return
		}
		d, err := time.Parse(http.TimeFormat, requestDate)
		if err != nil {
			panic(err)
		}
		if time.Now().Before(d) {
			//request from the future
			http.Error(w, "400 request postdates response", http.StatusBadRequest)
			return
		}
		if diff := time.Now().Sub(d); diff > 5*time.Minute {
			//stale request
			http.Error(w, "400 stale request", http.StatusBadRequest)
			return
		}
		//check Apps table to see whether the app exists
		appname := ps.ByName("appname")
		a, err := selectApp(appname)
		if err != nil {
			if err != sql.ErrNoRows {
				panic(err)
			}
			//replying with 401 here to avoid enumeration attacks
			fmt.Printf("%s resource not found during Auth, replying with a 401.\n", "App")
			notAuthorized(w, r)
			return
		}
		h, err := hex.DecodeString(r.Header.Get("Authorization"))
		if err != nil {
			//invalid Authorization, has to be a hex string
			fmt.Println(err)
			http.Error(w, "400 illegal characters in Authorization field", http.StatusBadRequest)
			return
		}
		var m []byte
		if r.Method == "PATCH" || r.Method == "POST" || r.Method == "PUT" {
			//other issues can still occur, we have to read the entire body to decide whether to throw away the request
			//so attacker can make us waste resources by sending large chunks of data
			payload, err := ioutil.ReadAll(r.Body)
			if err != nil {
				panic(err)
			}
			//creating a new reader so subsequent handlers have something to read from
			r.Body = ioutil.NopCloser(bytes.NewReader(payload))
			fmt.Println(string(payload))
			m = append(payload, []byte(requestDate)...)
		} else {
			//don't try to copy nil body
			m = []byte(requestDate)
		}

		//first check app hash, then check user hash
		if authApp(a, h, m) {
			// if authApp(r, ps, h) {
			fmt.Println("authenticated using app hash")
			protected(w, r, ps)
			return
		}

		//select username from {appname}Users, check hash
		//no matches -> create hash from username, check it
		//still no matches -> unauthorized

		userTable := appname + "Users"
		username := ps.ByName("username")
		u, err := selectUser(userTable, username)
		if err != nil {
			if err != sql.ErrNoRows {
				panic(err)
			}
		}

		if err == sql.ErrNoRows {
			//user resource doesn't exist
			if r.Method == "PUT" && strings.HasPrefix(r.URL.Path, "/api/v0/app/"+a.AppName+"/user/") {
				//hash username and check, it might be a request for user resource creation
				//hashing username using app hash
				hasher := hmac.New(sha256.New, []byte(a.Hash))
				hasher.Write([]byte(username))
				userHash := hasher.Sum(nil) //user specific secret
				authHasher := hmac.New(sha256.New, userHash)
				authHasher.Write(m)
				authHash := authHasher.Sum(nil)
				fmt.Printf("hash from the request: %x\n", h)
				fmt.Printf("authHash generated from generated userHash: %x\n", authHash)
				if hmac.Equal(h, authHash) {
					protected(w, r, ps)
					return
				}
				//unauthorized
				notAuthorized(w, r)
			}
		}

		authHasher := hmac.New(sha256.New, []byte(u.Hash))
		authHasher.Write(m)
		authHash := authHasher.Sum(nil)
		fmt.Printf("hash from the request: %x\n", h)
		fmt.Printf("authHash generated from userHash: %x\n", authHash)
		if hmac.Equal(h, authHash) {
			fmt.Println("user hash matches")
			protected(w, r, ps)
			return
		}

	})
}

func authApp(a RESTApp, hash []byte, m []byte) (authorized bool) {
	// appHash, err := hex.DecodeString(a.Hash)
	// if err != nil {
	// 	panic(err)
	// }
	authHasher := hmac.New(sha256.New, []byte(a.Hash))
	fmt.Println(string(m))
	authHasher.Write(m)
	authHash := authHasher.Sum(nil)
	fmt.Printf("hash from the request: %x\n", hash)
	fmt.Printf("authHash generated from appHash: %x\n", authHash)
	if hmac.Equal(hash, authHash) {
		return true
	}
	return false
}

func notAuthorized(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "401 not authorized", http.StatusUnauthorized)
}

//CreateUser handles creation of new user resource, it is called by PutUser when user resource doesn't exist yet.
func CreateUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var u RESTUser
	appname := ps.ByName("appname")
	username := ps.ByName("username")
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &u)
	if err != nil {
		//can't Unmarshal -> malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if !u.sanitize() {
		//malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	//check that the struct from r.Body contains the same username as the url params
	if u.Username != username {
		//url parameter doesn't match request payload
		http.Error(w, "400 url parameter doesn't match request payload", http.StatusBadRequest)
		return
	}
	//check whether username isn't already taken
	_, err = selectUser(appname+"Users", username)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err != sql.ErrNoRows {
		//err is nil, username take
		http.Error(w, "409 username already taken", http.StatusConflict)
		return
	}

	//lookup app hash and use it to create user specific secret
	a, err := selectApp(appname)
	if err != nil {
		//this should never happen since App entry is needed to get past auth handlers
		log.Fatal(err)
	}
	hasher := hmac.New(sha256.New, []byte(a.Hash))
	hasher.Write([]byte(username))
	u.Hash = fmt.Sprintf("%x", hasher.Sum(nil)) //user specific secret
	if u.Created == 0 {
		u.Created = time.Now().Unix()
	}
	userTable := appname + "Users"

	affected, err := insertUser(userTable, u)
	if err != nil {
		panic(err)
	}
	//create a default subscription for the new user
	//get active plan
	//use newSubFromPlan with the plan, insert the sub
	p, err := selectActivePlan(appname)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		//active plan doesn't exist, since no plans at all exist
		//i guess we can reply with 422 or 500, from the PanicHandler
		panic(err)
	}
	s := newSubFromPlan(p)
	affected2, err := insertSubNoTags(appname+"Subs", username, s)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Inserted %d rows.\n", affected+affected2)

	created, err := selectUser(userTable, username)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		//database works, but inserted value isnt there
		//this should never happen
		log.Fatal(err)
	}
	resp, err := json.Marshal(created)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	w.WriteHeader(http.StatusCreated)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource created and %d bytes written in response to %s request.\n", "User", written, "PUT")
}

//ModifyUser handles partial updates of existing user resource, it is called on PATCH requests.
func ModifyUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var u RESTUser
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &u)
	if err != nil {
		//can't Unmarshal -> malformed payload
		fmt.Println(err)
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if !u.sanitize() {
		//malformed payload
		fmt.Println(err)
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}

	username := ps.ByName("username")
	userTable := ps.ByName("appname") + "Users"
	//copying over the name from request URI, since partial update may not have it inside
	u.Username = username
	affected, err := updateUser(userTable, u)
	if err != nil {
		panic(err)
	}

	updated, err := selectUser(userTable, username)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
		//if a user was selected, it exists, and the patch merely didn't change anything
		//if no rows were selected, there is no such user
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(updated)
	if err != nil {
		log.Fatal(err)
	}
	//set header fields
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource modified %d rows affected and %d bytes written in response to %s request.\n", "User", affected, written, "PATCH")
}

//GetUser handles GET requests on user resource.
func GetUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	userTable := ps.ByName("appname") + "Users"
	username := ps.ByName("username")
	u, err := selectUser(userTable, username)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	//check for empty result set -> struct will be empty
	if err == sql.ErrNoRows {
		fmt.Printf("%s resource not found, replying with a 404.\n", "User")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(u)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("User resource found and %d bytes written in response to %s request.\n", written, "GET")
}

func GetAllUsers(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	users, err := selectAllUsers(ps.ByName("appname"))
	if err != nil {
		panic(err)
	}
	if len(users) == 0 {
		fmt.Printf("%s resources not found, replying with a 404.\n", "User")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(users)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resources found and %d bytes written in response to %s request.\n", "User", written, "GET")
}

//DeleteUser handles DELETE requests on a user resource
//Do not use unless intending to purge data! For retiring user account use PATCH instead to modify the field "retired".
func DeleteUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	affected, err := deleteUser(ps.ByName("appname"), ps.ByName("username"))
	if err != nil {
		panic(err)
	}
	if affected == 0 {
		http.NotFound(w, r)
		fmt.Println("User resource not found, no rows deleted.")
		return
	}
	w.WriteHeader(http.StatusNoContent)
	fmt.Printf("User resource deleted, %d rows deleted in response to %s request.\n", affected, "DELETE")
}

//GetSub handles GET requests on a subscription resource
func GetSub(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	username := ps.ByName("username")
	s, err := selectSub(appname, username, ps.ByName("subid"))
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	//we could also check sql.ErrNoRows instead of struct emptiness here, not sure whether there are any cases where they don't agree
	if err == sql.ErrNoRows {
		fmt.Printf("%s resource not found, replying with a 404.\n", "Subscription")
		http.NotFound(w, r)
		return
	}
	affected, err := insertTimestamp(appname+"Timestamps", username, s.SubID, RESTTimestamp{time.Now().Unix(), 0})
	if err != nil {
		panic(err)
	}
	resp, err := json.Marshal(s)
	if err != nil {
		//should never happen as we control the input to json.Marshal
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		//not sure when this would fail
		panic(err)
	}
	fmt.Printf("Subscription resource found, %d rows affected and %d bytes written in response to %s request.\n", affected, written, "GET")
}

//CreateSub handles POST requests on a subscription resource
func CreateSub(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	username := ps.ByName("username")
	var s RESTSub
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &s)
	if err != nil {
		//can't Unmarshal -> malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if !s.sanitize() {
		//malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	//check request body for planName, select plan from db
	p, err := selectPlan(appname, s.PlanName)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		//422 Unprocessable Entity
		http.Error(w, "422 no such plan", http.StatusUnprocessableEntity)
		return
	}
	//create sub with default values
	n := newSubFromPlan(p)
	//select all subs of user, check highest subid, n.SubID = highest + 1
	subs, err := selectAllSubs(appname, username)
	if err != nil {
		panic(err)
	}
	n.SubID = newSubID(subs)
	//add plan tags to the new sub
	n.Tags = p.Tags
	//overwrite with values from request body if needed
	if s.PlanName = ""; !s.isEmpty() {
		n = owSub(n, s, p)
	}
	//create a row in subTable with coresponding values
	affected, err := insertSub(appname, username, n)
	if err != nil {
		panic(err)
	}
	_, err = insertTimestamp(appname+"Timestamps", username, n.SubID, RESTTimestamp{n.Start, 0})
	if err != nil {
		panic(err)
	}
	fmt.Println("Subscription resource created, rows inserted:", affected)
	created, err := selectSub(appname, username, strconv.Itoa(n.SubID))
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		//database works, but inserted value isnt there
		//this should never happen
		log.Fatal(err)
	}
	resp, err := json.Marshal(created)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	w.Header().Set("Location", "/api/v0/"+appname+"/user/"+username+"/sub/"+strconv.Itoa(n.SubID))
	w.WriteHeader(http.StatusCreated)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%d bytes written in response to %s request.\n", written, "POST")
}

func newSubID(subs []RESTSub) int {
	var ID int
	for _, s := range subs {
		if ID < s.SubID {
			ID = s.SubID
		}
	}
	return ID + 1
}

func newSubFromPlan(p RESTPlan) RESTSub {
	var n RESTSub
	n.Level = p.Level
	n.PlanName = p.Name
	n.Price = p.Price
	n.Renew = p.Renew
	copy(n.Tags, p.Tags)
	start := time.Now()
	n.Start = start.Unix()
	end := start.Add(time.Duration(p.Length) * 24 * time.Hour)
	n.End = end.Unix()
	n.Created = n.Start
	n.Modified = n.Start
	return n
}

//note that the emptiness checks need to be there, to keep from overwriting fields that we want to use
func owSub(dest RESTSub, src RESTSub, p RESTPlan) RESTSub {
	if src.Start != 0 {
		dest.Start = src.Start
		//recalculate dest.End
		start := time.Unix(src.Start, 0)
		end := start.Add(time.Duration(p.Length) * 24 * time.Hour)
		dest.End = end.Unix()
	}
	if src.End != 0 {
		dest.End = src.End
	}
	if src.Level != "" {
		dest.Level = src.Level
	}
	if src.PlanName != "" {
		dest.PlanName = src.PlanName
	}
	if src.Tag != "" {
		dest.Tag = src.Tag
	}
	if src.Created != 0 {
		dest.Created = src.Created
	}
	if src.Modified != 0 {
		dest.Modified = src.Modified
	}
	if src.Price != 0 {
		dest.Price = src.Price
	}
	if src.Renew != false {
		dest.Renew = src.Renew
	}
	return dest
}

//ModifySub handles partial updates on subscription resources, it is called on PATCH requests.
func ModifySub(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	username := ps.ByName("username")
	//unmarshal into RESTSub, ommited fields from json will have zero values
	//call updateSub, zero values are skipped, original values retained
	//select the resulting row, marshal, write response, status etc.
	var s RESTSub
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &s)
	if err != nil {
		//can't Unmarshal -> malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if !s.sanitize() {
		//malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}

	s.SubID, err = strconv.Atoi(ps.ByName("subid"))
	if err != nil {
		panic(err)
	}
	affected, err := updateSub(appname, username, s)
	if err != nil {
		panic(err)
	}
	if s.Tag != "" {
		af, err := insertSubTag(appname+"SubTags", username, s.SubID, s.Tag)
		if err != nil {
			panic(err)
		}
		affected += af
	}
	af, err := insertTimestamp(appname+"Timestamps", username, s.SubID, RESTTimestamp{time.Now().Unix(), 0})
	if err != nil {
		panic(err)
	}
	affected += af
	updated, err := selectSub(appname, username, ps.ByName("subid"))
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
		//if a resource was selected, it exists, and the patch merely didn't change anything
		//if no rows were selected, there is no such resource
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(updated)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource modified and %d bytes written in response to %s request.\n", "Subscription", written, "PATCH")
}

//GetActiveSub handles GET requests for representation of the currently active subscription.
//It goes through all of user's subscriptions and returns the one that starts before and ends after today.
//If there are more active subscriptions, it responds with 409 Conflict and if there are none, it responds with 404 Not Found.
func GetActiveSub(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	//select all subs
	appname := ps.ByName("appname")
	subs, err := selectAllSubs(appname, ps.ByName("username"))
	if err != nil {
		panic(err)
	}
	s, err := getActiveSub(subs)
	if err == errNoSubs {
		//return 404
		http.NotFound(w, r)
		return
	} else if err == errMoreSubs {
		//return 409
		http.Error(w, "409 "+err.Error(), http.StatusConflict)
		return
	}
	_, err = insertTimestamp(appname+"Timestamps", ps.ByName("username"), s.SubID, RESTTimestamp{time.Now().Unix(), 0})
	if err != nil {
		panic(err)
	}
	resp, err := json.Marshal(s)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource found and %d bytes written in response to %s request.\n", "Active subscription", written, "GET")
}

func getActiveSub(subs []RESTSub) (RESTSub, error) {
	now := time.Now().Unix()
	var active RESTSub
	for _, s := range subs {
		if s.Start < now && s.End > now {
			if !active.isEmpty() {
				return active, errMoreSubs
			}
			active = s
		}
	}
	if active.isEmpty() {
		return active, errNoSubs
	}
	return active, nil
}

//ModifyActiveSub handles PATCH requests on the active subscription,
//it adds its SubID to the request and then calls ModifySub.
func ModifyActiveSub(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// get active sub, put the SubID into ps, then call ModifySub with w, r and ps
	subs, err := selectAllSubs(ps.ByName("appname"), ps.ByName("username"))
	if err != nil {
		panic(err)
	}
	active, err := getActiveSub(subs)
	if err == errNoSubs {
		http.NotFound(w, r)
		return
	} else if err == errMoreSubs {
		http.Error(w, "409 "+err.Error(), http.StatusConflict)
		return
	}
	var p httprouter.Param
	p.Key = "subid"
	p.Value = strconv.Itoa(active.SubID)
	ps = append(ps, p)
	ModifySub(w, r, ps)
}

//GetAllSubs handles GET requests for all subs of a specified user. It responds with json array or subscription objects.
//No timestamps are created for the subscriptions.
func GetAllSubs(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	subs, err := selectAllSubs(ps.ByName("appname"), ps.ByName("username"))
	if err != nil {
		panic(err)
	}
	if len(subs) == 0 {
		fmt.Printf("%s resources not found, replying with a 404.\n", "Subscription")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(subs)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resources found and %d bytes written in response to %s request.\n", "Subscription", written, "GET")
}

//maintenance function to renew subscriptions with renew set to true
//called periodically while the server is running
//if server is offline for a time, workaround is needed, such as saving the last time the function ran, and call it at least for every day, while advancing the time manually
func renewSubs(appname string, current time.Time) error {
	rows, err := db.Query(fmt.Sprintf("SELECT username, planName, end FROM `%s` WHERE (end - '%d') < '%d'", appname+"Subs", current.Unix(), time.Hour*24))
	if err != nil {
		return err
	}
	defer rows.Close()
	var users []string
	var planNames []string
	var ends []int64
	for rows.Next() {
		//copy to an array so we can close the connection
		var username, planName string
		var end int64
		err := rows.Scan(&username, &planName, &end)
		if err != nil {
			return err
		}
		users = append(users, username)
		planNames = append(planNames, planName)
		ends = append(ends, end)
	}
	err = rows.Err()
	if err != nil {
		return err
	}
	for i := 0; i < len(users); i++ {
		//select the plan
		p, err := selectPlan(appname, planNames[i])
		if err != nil {
			return err
		}
		//create new sub from plan
		s := newSubFromPlan(p)
		s.Start = ends[i] + 1
		newStart := time.Unix(s.Start, 0)
		newEnd := newStart.Add(time.Hour * 24 * time.Duration(p.Length))
		s.End = newEnd.Unix()
		//insert
		subs, err := selectAllSubs(appname, users[i])
		if err != nil {
			return err
		}
		s.SubID = newSubID(subs)
		_, err = insertSub(appname, users[i], s)
		if err != nil {
			return err
		}
	}
	return nil
}

//GetApp handles GET requests for the app information.
func GetApp(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	//no need to check for empty struct after select, since the app has to exist for the request to get through authentication
	a, err := selectApp(ps.ByName("appname"))
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		fmt.Printf("%s resource not found, replying with a 404.\n", "App")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(a)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource found and %d bytes written in response to %s request.\n", "App", written, "GET")
}

func AuthRegistration(protected httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		//check the supplied registration code against db
		code := r.Header.Get("Authorization")
		cc := couponcode.New(4, 4)
		validated, err := cc.Validate(code)
		if err != nil {
			//code is not valid, perhaps the user made a mistake inputting it
			//return Not Found with a hint
			http.Error(w, "404 "+err.Error(), http.StatusNotFound)
			return
		}
		_, err = selectRegistrationCode(validated)
		if err != nil {
			if err != sql.ErrNoRows {
				panic(err)
			}
			fmt.Printf("%s resource not found, replying with a 401.\n", "Registration Code")
			notAuthorized(w, r)
			return
		}
		//delete the code
		affected, err := deleteRegistrationCode(validated)
		if err != nil {
			panic(err)
		}
		fmt.Printf("Registration code accepted, %d rows affected.\n", affected)
		protected(w, r, ps)
	})
}

//CreateApp handles creation of the app resource and initialization of other required resources.
//It is called on PUT requests.
func CreateApp(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var a RESTApp
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &a)
	if err != nil {
		//can't Unmarshal -> malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if !a.sanitize() {
		//malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}

	if (a == RESTApp{}) {
		//required information missing
		http.Error(w, "400 request payload empty", http.StatusBadRequest)
		return
	}
	if a.AppName != ps.ByName("appname") {
		//url parameter doesn't match request payload
		http.Error(w, "400 url parameter doesn't match request payload", http.StatusBadRequest)
		return
	}
	if a.Secret == "" {
		http.Error(w, "400 secret field required", http.StatusBadRequest)
		return
	}
	if a.Password == "" {
		http.Error(w, "400 password field required", http.StatusBadRequest)
		return
	}

	//calculate hash
	salt := make([]byte, 32)
	_, err = rand.Read(salt)
	fmt.Println("salt is:", salt)
	hasher := hmac.New(sha256.New, salt)
	hasher.Write([]byte(a.Secret))
	a.Hash = fmt.Sprintf("%x", hasher.Sum(nil))

	affected, err := insertApp(a)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Inserted %d rows.\n", affected)
	err = initApp(ps.ByName("appname"), a.Password)
	if err != nil {
		panic(err)
	}
	updated, err := selectApp(ps.ByName("appname"))
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	resp, err := json.Marshal(updated)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	w.WriteHeader(http.StatusCreated)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource created and %d bytes written in response to %s request.\n", "App", written, "PUT")
}

//create adminTable, insert default admin account
//create userTable, planTable, subTable
//insert active plan into planTable
func initApp(appname, password string) error {
	err := createAppAdmins(appname)
	if err != nil {
		return err
	}
	var admin RESTAdmin
	admin.Login = "admin"
	admin.Password = password
	_, err = insertAdmin(appname+"Admins", admin)
	if err != nil {
		return err
	}
	err = createAppUsers(appname)
	if err != nil {
		return err
	}
	fmt.Println("i got here")
	err = createAppPlans(appname)
	if err != nil {
		return err
	}
	fmt.Println("i got here2")
	err = createAppPlanTags(appname)
	if err != nil {
		return err
	}
	var p RESTPlan
	p.Name = "free"
	p.Level = "free"
	p.Active = true
	fmt.Println("i got here3")
	_, err = insertPlan(appname, p)
	if err != nil {
		return err
	}
	fmt.Println("i got here4")
	err = createAppActivePlan(appname)
	if err != nil {
		return err
	}
	fmt.Println("i got here5")
	err = createAppActivePlanTags(appname)
	if err != nil {
		return err
	}
	fmt.Printf("Plan inserted: %#v\n", p)
	_, err = insertActivePlan(appname, p)
	if err != nil {
		return err
	}
	fmt.Println("i got here6")
	err = createAppSubs(appname)
	if err != nil {
		return err
	}
	fmt.Println("i got here7")
	err = createAppSubTags(appname)
	if err != nil {
		return err
	}
	fmt.Println("i got here7")
	err = createAppTimestamps(appname)
	if err != nil {
		return err
	}
	fmt.Println("i got here8")
	err = createAppDiscounts(appname)
	if err != nil {
		return err
	}
	fmt.Println("i got here9")
	err = createAppCodes(appname)
	if err != nil {
		return err
	}
	return nil
}

//ModifyApp handles partial updates of app resource, it is called on PATCH requests.
//It responds with the updated resource.
func ModifyApp(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	//unmarshal, update db row in Apps table
	//select updated, return
	var a RESTApp
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &a)
	if err != nil {
		//can't Unmarshal -> malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if !a.sanitize() {
		//malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}

	_, err = selectApp(ps.ByName("appname"))
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
		fmt.Printf("%s resource not found, replying with a 404.\n", "App")
		http.NotFound(w, r)
		return
	}

	var affected int64
	if a.Secret != "" {
		//recalculate hash
		//TODO we should only store the resulting hash, we don't need to store the secret nor the salt,
		//TODO so we can remove the secret column from db
		salt := make([]byte, 32)
		_, err = rand.Read(salt)
		fmt.Println("salt is:", salt)
		hasher := hmac.New(sha256.New, salt)
		hasher.Write([]byte(a.Secret))
		a.Hash = fmt.Sprintf("%x", hasher.Sum(nil))
		//recalculate user specific secrets
		a.AppName = ps.ByName("appname")
		af, err := recalculateUserHashes(a)
		if err != nil {
			panic(err)
		}
		affected += af
	}
	a.AppName = ps.ByName("appname")
	af, err := updateApp(a)
	if err != nil {
		panic(err)
	}
	affected += af
	fmt.Printf("Inserted %d rows.\n", affected)
	updated, err := selectApp(a.AppName)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
		//if a user was selected, it exists, and the patch merely didn't change anything
		//if no rows were selected, there is no such user
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(updated)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s resource modified and %d bytes written in response to %s request.\n", "App", written, "PATCH")
}

// this could be optimized with a better SQL statement and/or goroutines
func recalculateUserHashes(a RESTApp) (int64, error) {
	users, err := selectAllUsers(a.AppName)
	if err != nil {
		if err != sql.ErrNoRows {
			return 0, err
		}
		//no users
		return 0, nil
	}
	var affected int64
	for _, u := range users {
		hasher := hmac.New(sha256.New, []byte(a.Hash))
		hasher.Write([]byte(u.Username))
		u.Hash = fmt.Sprintf("%x", hasher.Sum(nil)) //user specific secret
		af, err := updateUser(a.AppName+"Users", u)
		if err != nil {
			return 0, err
		}
		affected += af
	}
	return affected, nil
}

//DeleteApp handles DELETE requests. It delets all app information including users, subscriptions, plans, and admins.
//Use with caution.
func DeleteApp(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	//TODO make this use a different hash than the normal app auth
	//probably make this only accessible through the web interface (how?)

	//delete subTable, planTable, userTable, adminTable
	//then call deleteApp to delete the respective row in the Apps table
	appname := ps.ByName("appname")
	err := dropAppCodes(appname)
	if err != nil {
		panic(err)
	}
	err = dropAppDiscounts(appname)
	if err != nil {
		panic(err)
	}
	err = dropAppTimestamps(appname)
	if err != nil {
		panic(err)
	}
	err = dropAppSubTags(appname)
	if err != nil {
		panic(err)
	}
	err = dropAppSubs(appname)
	if err != nil {
		panic(err)
	}
	err = dropAppActivePlanTags(appname)
	if err != nil {
		panic(err)
	}
	err = dropAppActivePlan(appname)
	if err != nil {
		panic(err)
	}
	err = dropAppPlanTags(appname)
	if err != nil {
		panic(err)
	}
	err = dropAppPlans(appname)
	if err != nil {
		panic(err)
	}
	err = dropAppUsers(appname)
	if err != nil {
		panic(err)
	}
	err = dropAppAdmins(appname)
	if err != nil {
		panic(err)
	}
	affected, err := deleteApp(appname)
	if err != nil {
		panic(err)
	}
	w.WriteHeader(http.StatusNoContent)
	fmt.Printf("%s request carried out.\nApp information deleted, %d rows deleted in the app table.\nApp users, subs, plans and admins deleted.\n", "DELETE", affected)
}

//GetDiscount handles GET requests for discount resource.
func GetDiscount(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	planName := ps.ByName("planname")
	discountName := ps.ByName("discountname")
	d, err := selectDiscount(ps.ByName("appname")+"Discounts", planName, discountName)
	//check for empty result set -> struct will be empty
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		fmt.Printf("%s resource not found, replying with a 404.\n", "Subscription")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(d)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Discount resource found and %d bytes written in response to %s request.\n", written, "GET")
}

//CreateDiscount handles requests for creation of new discount resources.
func CreateDiscount(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	discountTable := ps.ByName("appname") + "Discounts"
	var d RESTDiscount
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &d)
	if err != nil {
		//can't Unmarshal -> malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if !d.sanitize() {
		//malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}

	if (d == RESTDiscount{}) {
		//required information missing
		http.Error(w, "400 request payload empty", http.StatusBadRequest)
		return
	}
	if d.PlanName != ps.ByName("planname") {
		//url parameter doesn't match request payload
		http.Error(w, "400 url parameter doesn't match request payload", http.StatusBadRequest)
		return
	}
	if d.DiscountName != ps.ByName("discountname") {
		//url parameter doesn't match request payload
		http.Error(w, "400 url parameter doesn't match request payload", http.StatusBadRequest)
		return
	}
	_, err = selectDiscount(discountTable, d.PlanName, d.DiscountName)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err != sql.ErrNoRows {
		//discount with identical name
		http.Error(w, "409 name not available, choose a different one", http.StatusConflict)
		return
	}
	affected, err := insertDiscount(discountTable, d)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Inserted %d rows.\n", affected)
	created, err := selectDiscount(discountTable, d.PlanName, d.DiscountName)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		//database works, but inserted value isnt there
		//this should never happen
		log.Fatal(err)
	}
	resp, err := json.Marshal(created)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	w.WriteHeader(http.StatusCreated)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource created and %d bytes written in response to %s request.\n", "Discount", written, "PUT")
}

//ModifyDiscount handles partial updates of the discount resource, it is called on PATCH requests.
func ModifyDiscount(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	discountTable := ps.ByName("appname") + "Discounts"
	var d RESTDiscount
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &d)
	if err != nil {
		//can't Unmarshal -> malformed payload
		fmt.Println(err)
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if !d.sanitize() {
		//malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}

	if (d == RESTDiscount{}) {
		//required information missing
		http.Error(w, "400 request payload empty", http.StatusBadRequest)
		return
	}
	d.PlanName = ps.ByName("planname")
	d.DiscountName = ps.ByName("discountname")
	affected, err := updateDiscount(discountTable, d)
	if err != nil {
		panic(err)
	}
	if affected == 0 {
		http.NotFound(w, r)
		return
	}
	fmt.Printf("Updated %d rows.\n", affected)
	updated, err := selectDiscount(discountTable, d.PlanName, d.DiscountName)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
		//if a resource was selected, it exists, and the patch merely didn't change anything
		//if no rows were selected, there is no such resource
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(updated)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource modified and %d bytes written in response to %s request.\n", "Discount", written, "PATCH")
}

//DeleteDiscount handles DELETE requests for the discount resource
func DeleteDiscount(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	affected, err := deleteDiscount(ps.ByName("appname")+"Discounts", ps.ByName("planname"), ps.ByName("discountname"))
	if err != nil {
		panic(err)
	}
	if affected == 0 {
		http.NotFound(w, r)
		fmt.Println("Discount resource not found, no rows deleted.")
		return
	}
	w.WriteHeader(http.StatusNoContent)
	fmt.Printf("%s resource deleted, %d rows deleted in response to %s request", "Discount", affected, "DELETE")
}

//GetPlanDiscounts handles GET requests for a set of discount resources associated with a plan.
func GetPlanDiscounts(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	planName := ps.ByName("planname")
	discounts, err := selectPlanDiscounts(ps.ByName("appname")+"Discounts", planName)
	if err != nil {
		panic(err)
	}
	//check for empty result set -> struct will be empty
	if len(discounts) == 0 {
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(discounts)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Discount resources of a plan found and %d bytes written in response to %s request.\n", written, "GET")
}

//GetAppDiscounts handles GET requests for a set of discount resources associated with an app.
func GetAppDiscounts(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	discounts, err := selectAppDiscounts(ps.ByName("appname") + "Discounts")
	if err != nil {
		panic(err)
	}
	//check for empty result set -> struct will be empty
	if len(discounts) == 0 {
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(discounts)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Discount resources of an app found and %d bytes written in response to %s request.\n", written, "GET")
}

//GenerateDiscountCodes generates a specified number of discount codes, and returns them in an array.
func GenerateDiscountCodes(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	planName := ps.ByName("planname")
	discountName := ps.ByName("discountname")
	queryValues := r.URL.Query()
	count, _ := strconv.Atoi(queryValues.Get("count"))
	d, err := selectDiscount(appname+"Discounts", planName, discountName)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		fmt.Printf("%s resource not found, replying with a 404.\n", "Discount")
		http.NotFound(w, r)
		return
	}
	//generate codes
	plainCodes := make([]string, count)
	codes := make(map[string]RESTDiscountCode, count)
	cc := couponcode.New(4, 4)
	//iterate until the maps hold count elements each
	for i := 0; i < len(plainCodes); i++ {
		code := cc.Generate()
		if _, ok := codes[code]; ok {
			//generated a duplicate
			i--
			continue
		}
		exists, err := existsDiscountCode(appname, code)
		if err != nil {
			panic(err)
		}
		if exists {
			//duplicate already in the DB
			i--
			continue
		}
		plainCodes[i] = code
		var c RESTDiscountCode
		c.Code = code
		c.PlanName = planName
		c.DiscountName = discountName
		c.Percentage = d.Percentage
		codes[code] = c
	}
	affected, err := insertDiscountCodes(appname, codes)
	if err != nil {
		panic(err)
	}
	resp, err := json.Marshal(plainCodes)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Codes generated, %d rows affected and %d bytes written in response to %s request.\n", affected, written, "POST")
}

//CheckDiscountCode checks validity of the supplied code, returns information about the discount on success.
//Called on GET requests
func CheckDiscountCode(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	code := ps.ByName("code")
	cc := couponcode.New(4, 4)
	validated, err := cc.Validate(code)
	if err != nil {
		//code is not valid, perhaps the user made a mistake inputting it
		//return Not Found with a hint
		http.Error(w, "404 "+err.Error(), http.StatusNotFound)
		return
	}
	//use validated code for query, ambiguous characters are replaced, formatted to lowercase, etc.
	c, err := selectDiscountCode(ps.ByName("appname"), validated)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		fmt.Printf("%s resource not found, replying with a 404.\n", "Code")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(c)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Discount code resource found, %d bytes written in response to %s request.\n", written, "GET")
}

//DeleteDiscountCode invalidates a discount code, called on DELETE requests.
func DeleteDiscountCode(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	affected, err := deleteDiscountCode(ps.ByName("appname"), ps.ByName("code"))
	if err != nil {
		panic(err)
	}
	if affected == 0 {
		fmt.Println("Discount code resource not found, no rows deleted, replying with a 404.")
		http.NotFound(w, r)
		return
	}
	w.WriteHeader(http.StatusNoContent)
	fmt.Printf("%s resource deleted, %d rows deleted in response to %s request", "Discount code", affected, "DELETE")
}

//GetAdmin handles GET requests for admin resource.
func GetAdmin(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	adminTable := ps.ByName("appname") + "Admins"
	admin, err := selectAdmin(adminTable, ps.ByName("login"))
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		fmt.Printf("%s resource not found, replying with a 404.\n", "Admin")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(admin)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource found and %d bytes written in response to %s request.\n", "Admin", written, "GET")
}

//CreateAdmin handles requests for creation of new admin resources.
func CreateAdmin(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	//unmarshal,
	adminTable := ps.ByName("appname") + "Admins"
	var admin RESTAdmin
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &admin)
	if err != nil {
		//can't Unmarshal -> malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if (admin == RESTAdmin{}) {
		//required information missing
		http.Error(w, "400 request payload empty", http.StatusBadRequest)
		return
	}
	if admin.Login != ps.ByName("login") {
		//url parameter doesn't match request payload
		http.Error(w, "400 url parameter doesn't match request payload", http.StatusBadRequest)
		return
	}
	if admin.Password == "" {
		http.Error(w, "400 password field required", http.StatusBadRequest)
		return
	}
	_, err = selectAdmin(adminTable, admin.Login)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err != sql.ErrNoRows {
		//err is nil
		//admin with identical login field already exists
		http.Error(w, "409 login not available, choose a different one", http.StatusConflict)
		return
	}
	affected, err := insertAdmin(adminTable, admin)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Inserted %d rows.\n", affected)
	created, err := selectAdmin(adminTable, admin.Login)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		//database works, but inserted value isnt there
		//this should never happen
		log.Fatal(err)
	}
	resp, err := json.Marshal(created)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	w.WriteHeader(http.StatusCreated)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource created and %d bytes written in response to %s request.\n", "Admin", written, "PUT")
}

//ModifyAdmin handles partial updates of the admin resource, it is called on PATCH requests.
func ModifyAdmin(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	adminTable := ps.ByName("appname") + "Admins"
	var admin RESTAdmin
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &admin)
	if err != nil {
		//can't Unmarshal -> malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if !admin.sanitize() {
		//malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}

	if (admin == RESTAdmin{}) {
		//required information missing
		http.Error(w, "400 request payload empty", http.StatusBadRequest)
		return
	}
	if admin.Password == "" {
		http.Error(w, "400 password field required", http.StatusBadRequest)
		return
	}
	admin.Login = ps.ByName("login")
	affected, err := updateAdmin(adminTable, admin)
	if err != nil {
		panic(err)
	}
	if affected == 0 {
		http.NotFound(w, r)
		return
	}
	fmt.Printf("Updated %d rows.\n", affected)
	updated, err := selectAdmin(adminTable, admin.Login)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
		//if a resource was selected, it exists, and the patch merely didn't change anything
		//if no rows were selected, there is no such resource
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(updated)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource modified and %d bytes written in response to %s request.\n", "Admin", written, "PATCH")
}

//DeleteAdmin handles DELETE requests for the admin resource
func DeleteAdmin(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	affected, err := deleteAdmin(ps.ByName("appname")+"Admins", ps.ByName("login"))
	if err != nil {
		panic(err)
	}
	if affected == 0 {
		http.NotFound(w, r)
		fmt.Println("Admin resource not found, no rows deleted.")
		return
	}
	w.WriteHeader(http.StatusNoContent)
	fmt.Printf("%s resource deleted, %d rows deleted in response to %s request.\n", "Admin", affected, "DELETE")
}

//GetPlan handles GET requests for the plan resource
func GetPlan(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	planName := ps.ByName("planname")
	p, err := selectPlan(appname, planName)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		fmt.Printf("%s resource not found, replying with a 404.\n", "Plan")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(p)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource found and %d bytes written in response to %s request.\n", "Plan", written, "GET")
}

//CreatePlan handles the creation of new plans, it is called on POST requests.
func CreatePlan(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	//unmarshal
	//select all plans, get new plan id
	//augment the struct with the new plan id, insert
	//get inserted plan
	//marshal, send back the response
	appname := ps.ByName("appname")
	planTable := appname + "Plans"
	planName := ps.ByName("planname")
	var p RESTPlan
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &p)
	if err != nil {
		//can't Unmarshal -> malformed payload
		fmt.Println(err)
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if !p.sanitize() {
		//malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}

	//check for malformed request
	if p.isEmpty() {
		//required information missing
		http.Error(w, "400 request payload empty", http.StatusBadRequest)
		return
	}
	if p.Name != planName {
		http.Error(w, "400 url parameter doesn't match request payload", http.StatusBadRequest)
		return
	}
	if p.Level == "" {
		http.Error(w, "400 level must be specified", http.StatusBadRequest)
		return
	}
	//check for naming conflict
	plans, err := selectAllPlansNoTags(planTable)
	if err != nil {
		panic(err)
	}
	for _, n := range plans {
		if n.Name == p.Name {
			//plan with identical name field already exists
			http.Error(w, "409 name not available, choose a different one", http.StatusConflict)
			return
		}
	}
	affected, err := insertPlan(appname, p)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Inserted %d rows.\n", affected)
	if p.Active {
		af, err := insertActivePlan(appname, p)
		if err != nil {
			panic(err)
		}
		fmt.Println("Active plan updated.")
		affected += af
	}
	created, err := selectPlan(appname, p.Name)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		//database works, but inserted value isnt there
		//this should never happen
		log.Fatal(err)
	}
	resp, err := json.Marshal(created)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	w.WriteHeader(http.StatusCreated)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource created and %d bytes written in response to %s request.\n", "Plan", written, "POST")
}

//ModifyPlan handles partial updates of the plan resource, it is called on PATCH requests.
func ModifyPlan(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	//unmarshal, update
	//ommited fields retain original value, PlanName cannot be changed, therefore if it is in the request body, it is ignored
	//select updated, send response
	appname := ps.ByName("appname")
	planName := ps.ByName("planname")
	var p RESTPlan
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(b, &p)
	if err != nil {
		//can't Unmarshal -> malformed payload
		fmt.Println(err)
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	if !p.sanitize() {
		//malformed payload
		http.Error(w, "400 request payload malformed", http.StatusBadRequest)
		return
	}
	//copy the name over from URI, in case it wasn't already there
	p.Name = planName
	affected, err := updatePlan(appname, p)
	if err != nil {
		panic(err)
	}
	updated, err := selectPlan(appname, p.Name)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
		//if a resource was selected, it exists, and the patch merely didn't change anything
		//if no rows were selected, there is no such resource
		http.NotFound(w, r)
		return
	}
	if updated.Active {
		//if Active is set (or was set before and didn't change now), replicate the updated plan into the active plan slot
		//delete anything there first (could also replace, to make it atomic but we are going to use an external mutex anyway)
		af, err := deleteActivePlan(appname)
		if err != nil {
			panic(err)
		}
		affected += af
		af, err = insertActivePlan(appname, updated)
		if err != nil {
			panic(err)
		}
		affected += af
	}
	resp, err := json.Marshal(updated)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource modified and %d bytes written in response to %s request.\n", "Plan", written, "PATCH")
}

//DeletePlan handles DELETE requests on plan resources.
func DeletePlan(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	planName := ps.ByName("planname")
	if planName == "active" {
		//can't delete active plan, it's just a duplicate of another plan
		http.Error(w, "400 invalid planname", http.StatusBadRequest)
		return
	}
	var affected int64
	//check whether the plan isnt also a active plan, if yes, delete the active plan too
	active, err := selectActivePlan(appname)
	if err != nil {
		if err != errNoPlans {
			panic(err)
		}
	}
	if err != sql.ErrNoRows {
		//err is nil
		if active.Name == planName {
			//delete the active plan
			af, err := deleteActivePlan(appname)
			if err != nil {
				panic(err)
			}
			affected += af
		}
	}
	af, err := deletePlan(appname, planName)
	if err != nil {
		panic(err)
	}
	affected += af
	w.WriteHeader(http.StatusNoContent)
	fmt.Printf("%s resource deleted, %d rows changed in response to %s request.\n", "Plan", affected, "DELETE")
}

//GetActivePlan handles GET requests for the (fictional) active plan resouce.
func GetActivePlan(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	p, err := selectActivePlan(appname)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	if err == sql.ErrNoRows {
		fmt.Printf("%s resource not found, replying with a 404.\n", "Plan")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(p)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource found and %d bytes written in response to %s request.\n", "Active Plan", written, "GET")
}

func GetAllPlans(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	plans, err := selectAllPlans(ps.ByName("appname"))
	if err != nil {
		panic(err)
	}
	if len(plans) == 0 {
		fmt.Printf("%s resources not found, replying with a 404.\n", "Plan")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(plans)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resources found and %d bytes written in response to %s request.\n", "Plan", written, "GET")
}

//GetTimestamps handles GET requests for timestamps of specified subscription and user. It returns an array of timestamp objects.
func GetTimestamps(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ts, err := selectTimestamps(ps.ByName("appname")+"Timestamps", ps.ByName("username"), ps.ByName("subid"))
	if err != nil {
		panic(err)
	}
	if ts == nil {
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(ts)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource found and %d bytes written in response to %s request.\n", "Timestamps", written, "GET")
}

func GetTags(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	planName := ps.ByName("planname")
	subID := ps.ByName("subid")
	var tags []string
	var err error
	var noun string
	if planName == "" && subID == "" {
		//error
	}
	if planName == "" && subID != "" {
		tags, err = selectAllSubTags(appname+"SubTags", ps.ByName("username"), subID)
		if err != nil {
			panic(err)
		}
		noun = "Plan"
	} else {
		tags, err = selectAllPlanTags(appname+"PlanTags", planName)
		if err != nil {
			panic(err)
		}
		noun = "Sub"
	}
	if len(tags) == 0 {
		fmt.Printf("%s resource not found, replying with a 404.\n", "Plan tag")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(tags)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource found and %d bytes written in response to %s request.\n", noun+" Tags", written, "GET")
}

func CheckTag(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	planName := ps.ByName("planname")
	subID := ps.ByName("subid")
	tag := ps.ByName("tag")
	var found bool
	var err error
	var noun string
	if planName == "" && subID == "" {
		//wrong URI path
	}
	if planName == "" && subID != "" {
		username := ps.ByName("username")
		found, err = hasSubTag(appname+"SubTags", username, subID, tag)
		if err != nil {
			panic(err)
		}
		noun = "Plan"
	} else {
		found, err = checkPlanTag(appname, planName, tag)
		if err != nil {
			panic(err)
		}
		noun = "Sub"
	}
	if !found {
		fmt.Printf("%s resource not found, replying with a 404.\n", noun+" tag")
		http.NotFound(w, r)
		return
	}
	resp, err := json.Marshal(RESTTag{Tag: tag})
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource found and %d bytes written in response to %s request.\n", noun+" Tag", written, "GET")
}

func checkPlanTag(appname string, planName string, tag string) (bool, error) {
	tags, err := selectAllPlanTags(appname+"PlanTags", planName)
	if err != nil {
		return false, err
	}
	found := false
	for _, t := range tags {
		if t == tag {
			found = true
			break
		}
	}
	return found, nil
}

func CreateTag(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	planName := ps.ByName("planname")
	subID := ps.ByName("subid")
	tag := ps.ByName("tag")
	var affected int64
	var noun string
	if planName == "" && subID == "" {
		//wrong URI path
	}
	if planName == "" && subID != "" {
		ID, err := strconv.Atoi(subID)
		if err != nil {
			panic(err)
		}
		username := ps.ByName("username")
		af, err := insertSubTag(appname+"SubTags", username, ID, tag)
		if err != nil {
			panic(err)
		}
		affected += af
		noun = "Plan"
	} else {
		var arr []string
		arr = append(arr, tag)
		p := RESTPlan{RESTPlanNoArr: RESTPlanNoArr{Name: planName}, Tags: arr}
		af, err := insertPlanTags(appname+"PlanTags", p)
		if err != nil {
			panic(err)
		}
		affected += af
		noun = "Sub"
	}
	if affected == 0 {
		fmt.Printf("%s resource already exists, replying with a 409.\n", "Tag")
		http.Error(w, "409 tag already exists", http.StatusConflict)
		return
	}
	w.WriteHeader(http.StatusCreated)
	fmt.Printf("%s resource created and %d rows affected in response to %s request.\n", noun+" tag", affected, "PUT")
}

func DeleteTag(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	planName := ps.ByName("planname")
	subID := ps.ByName("subid")
	tag := ps.ByName("tag")
	var affected int64
	var noun string
	if planName == "" && subID == "" {
		//wrong URI path
	}
	if planName == "" && subID != "" {
		username := ps.ByName("username")
		af, err := deleteSubTag(appname+"SubTags", username, subID, tag)
		if err != nil {
			panic(err)
		}
		affected += af
		noun = "Plan"
	} else {
		af, err := deletePlanTag(appname+"PlanTags", planName, tag)
		if err != nil {
			panic(err)
		}
		affected += af
		noun = "Sub"
	}
	w.WriteHeader(http.StatusNoContent)
	fmt.Printf("%s resource deleted and %d rows affected in response to %s request.\n", noun+" tag", affected, "DELETE")
}
