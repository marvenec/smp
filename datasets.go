package main

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"strconv"
	"time"

	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/julienschmidt/httprouter"
)

//SanitizeQueryParams parses the URL query params and ensures that they are well formed. It should be called before dataset handlers in the handler call chain.
func SanitizeQueryParams(protected httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		//sanitize query params
		//for now just error on invalid
		queryValues := r.URL.Query()
		for param, value := range queryValues {
			//maybe we can just loop over them all, but in case there are any params that aren't numeric, we need to handle them in a separate case
			switch param {
			case "days", "offset", "activity", "inactivity", "interval", "threshold":
				//assuming single value per key
				if len(value) > 1 {
					//expected a single value
					//invalid url query params
					http.Error(w, "400 invalid query params", http.StatusBadRequest)
					return
				}
				if !isNumeric(value[0]) {
					//invalid url query params
					http.Error(w, "400 invalid query params", http.StatusBadRequest)
					return
				}
			case "names":
				if len(value) > 1 {
					//expected a single value
					//invalid url query params
					http.Error(w, "400 invalid query params", http.StatusBadRequest)
					return
				}
				if _, err := strconv.ParseBool(value[0]); err != nil {
					//value is not convertible to bool
					http.Error(w, "400 invalid query params", http.StatusBadRequest)
					return
				}
			default:
				//unexpected param
				http.Error(w, "400 invalid query params", http.StatusBadRequest)
				return
			}
		}
		//check tag url param
		fmt.Println(r.URL.Path)
		if p := ps.ByName("appname"); p != "" {
			fmt.Println("appname: ", p)
		}
		if p := ps.ByName("tag"); p != "" {
			fmt.Println("tag: ", p)
		}

		w.Header().Set("Access-Control-Allow-Origin", "*")
		protected(w, r, ps)
	})
}

func countNewUsers(appname string, days, offset int) ([]int, error) {
	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	first := last.AddDate(0, 0, -days)
	//select count users with created in between first and last
	//do a loop, count users for one day
	var counts []int
	for i := 0; i < days; i++ {
		next := first.AddDate(0, 0, 1)
		var c int
		err := db.QueryRow(fmt.Sprintf("SELECT COUNT(*) FROM `%s` WHERE created >= '%d' AND created < '%d'", appname+"Users", first.Unix(), next.Unix())).Scan(&c)
		if err != nil {
			return nil, err
		}
		counts = append(counts, c)
		first = next
	}
	//first should be last now
	return counts, nil
}

//GetNewUsers returns an array of new user counts per day.
func GetNewUsers(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	queryValues := r.URL.Query()
	days, _ := strconv.Atoi(queryValues.Get("days"))
	offset, _ := strconv.Atoi(queryValues.Get("offset"))
	counts, err := countNewUsers(ps.ByName("appname"), days, offset)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := json.Marshal(&counts)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("New users counted and %d bytes written in response to %s request.\n", written, "GET")
}

func countActiveUsers(appname string, days, offset, activity int) ([]int, error) {
	//select all in the timeframe of interest from timestamps, load into a map
	//for i < days, loop over results
	//for each user, increment a counter for every timestamp during a week
	//if counter > activity => user is active, increment count for that day
	//(the counter always belongs to the last day of the week being checked)

	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	//need extra 6 days before the first day, since activity is calculated over weeks
	first := last.AddDate(0, 0, -days-6)

	uses, err := timestampsPeriod(appname, first, last)
	if err != nil {
		return nil, err
	}
	//naive implementation.. most likely the place to optimize if performance is desired
	//e.g. do it in one pass somehow, instead of n = days passes
	end := first.AddDate(0, 0, 7)
	var counts []int
	for i := 0; i < days; i++ {
		startU := first.Unix()
		endU := end.Unix()
		active := 0
		for _, times := range uses {
			weekly := 0
			for _, t := range times {
				if t > startU && t < endU {
					weekly++
				}
			}
			if weekly >= activity {
				active++
			}
		}
		counts = append(counts, active)
		first = first.AddDate(0, 0, 1)
		end = end.AddDate(0, 0, 1)
	}
	return counts, nil
}

func timestampsPeriod(appname string, first, last time.Time) (map[string][]int64, error) {
	uses := make(map[string][]int64)
	rows, err := db.Query(fmt.Sprintf("SELECT username, time FROM `%s` WHERE time >= '%d' AND time < '%d'", appname+"Timestamps", first.Unix(), last.Unix()))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var u string
		var t int64
		err := rows.Scan(&u, &t)
		if err != nil {
			return nil, err
		}
		uses[u] = append(uses[u], t)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return uses, nil
}

//GetActiveUsers returns an array of active user counts per day.
func GetActiveUsers(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	queryValues := r.URL.Query()
	days, _ := strconv.Atoi(queryValues.Get("days"))
	offset, _ := strconv.Atoi(queryValues.Get("offset"))
	activity, _ := strconv.Atoi(queryValues.Get("activity"))
	//activity defaults to 5 uses
	if activity == 0 {
		activity = 5
	}
	counts, err := countActiveUsers(ps.ByName("appname"), days, offset, activity)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := json.Marshal(&counts)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Active users counted and %d bytes written in response to %s request.\n", written, "GET")
}

func newUserNames(appname string, first, last time.Time) ([]string, error) {
	rows, err := db.Query(fmt.Sprintf("SELECT username FROM `%s` WHERE created >= '%d' AND created < '%d'", appname+"Users", first.Unix(), last.Unix()))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var users []string
	for rows.Next() {
		var u string
		err := rows.Scan(&u)
		if err != nil {
			return nil, err
		}
		users = append(users, u)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return users, nil
}

func countLostUsers(appname string, days, offset, activity int) ([]int, error) {
	//need one extra day so we can it off later when calculating lostCounts
	noise := 1
	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	first := last.AddDate(0, 0, -days-6-noise)
	uses, err := timestampsPeriod(appname, first, last)
	if err != nil {
		return nil, err
	}
	//for each day, do this:
	//get names of users to delete, delete them
	//then count the active users normally
	var activeCounts []int
	end := first.AddDate(0, 0, 7)
	for i := 0; i < days+noise; i++ {
		m := make(map[string][]int64, len(uses))
		for k, v := range uses {
			m[k] = v
		}
		newUsers, err := newUserNames(appname, first, last)
		if err != nil {
			return nil, err
		}
		for _, n := range newUsers {
			m[n] = nil
		}

		startU := first.Unix()
		endU := end.Unix()
		active := 0
		for _, times := range m {
			weekly := 0
			for _, t := range times {
				if t > startU && t < endU {
					weekly++
				}
			}
			if weekly >= activity {
				active++
			}
		}
		activeCounts = append(activeCounts, active)
		first = first.AddDate(0, 0, 1)
		end = end.AddDate(0, 0, 1)
	}
	//to get a figure of users lost per day, subtract the previous value
	lostCounts := make([]int, days)
	for i, c := range activeCounts {
		if i > 0 {
			lostCounts[i-noise] = c - activeCounts[i-1]
		}
		//ignore first datapoint
	}
	return lostCounts, nil
}

//GetLostUsers returns an array of lost user counts per day
func GetLostUsers(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	queryValues := r.URL.Query()
	days, _ := strconv.Atoi(queryValues.Get("days"))
	offset, _ := strconv.Atoi(queryValues.Get("offset"))
	activity, _ := strconv.Atoi(queryValues.Get("activity"))
	//activity defaults to 5 uses
	if activity == 0 {
		activity = 5
	}
	counts, err := countLostUsers(ps.ByName("appname"), days, offset, activity)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := json.Marshal(&counts)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Lost users counted and %d bytes written in response to %s request.\n", written, "GET")
}

func countPayingUsers(appname string, days, offset, threshold int) ([]int, error) {
	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	first := last.AddDate(0, 0, -days)

	m, err := subsPeriod(appname, first, last)
	if err != nil {
		return nil, err
	}

	var counts []int
	//naive, iterating over the whole map for each day
	for i := 0; i < days; i++ {
		day := first.Unix()
		users := 0
		paying := 0
		for _, subs := range m {
			exists := false
			var spent float64
			//add up the price per month of all subscriptions, compare total against threshold
			for _, sub := range subs {
				if sub.Start < day && sub.End >= day {
					exists = true
					d := time.Unix(sub.End, 0).Sub(time.Unix(sub.Start, 0))
					length := math.Ceil(d.Hours() / 24)
					//avoid division by zero
					if length == 0 {
						continue
					}
					spent += sub.Price / length * 30
				}
			}
			if exists {
				users++
			}
			if spent >= float64(threshold) {
				paying++
			}
		}
		//avoid division by zero
		if users == 0 {
			counts = append(counts, 0)
			first = first.AddDate(0, 0, 1)
			continue
		}
		percent := paying * 100 / users
		counts = append(counts, percent)
		first = first.AddDate(0, 0, 1)
	}
	return counts, nil
}

//GetPayingUsers returns an array of paying user percentages per day.
func GetPayingUsers(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	//for now we assume that users paid the price specified in the plan
	queryValues := r.URL.Query()
	days, _ := strconv.Atoi(queryValues.Get("days"))
	offset, _ := strconv.Atoi(queryValues.Get("offset"))
	threshold, _ := strconv.Atoi(queryValues.Get("threshold"))
	//threshold defaults to 5
	if threshold == 0 {
		threshold = 5
	}
	counts, err := countPayingUsers(ps.ByName("appname"), days, offset, threshold)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := json.Marshal(&counts)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Paying users counted and %d bytes written in response to %s request.\n", written, "GET")
}

func simpleRound(f float64) float64 {
	return math.Floor(f + .5)
}

func countSubLevels(appname string, days, offset int) ([]map[string]int, error) {
	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	first := last.AddDate(0, 0, -days)

	m, err := subsPeriod(appname, first, last)
	if err != nil {
		return nil, err
	}

	//for every day, go through the map and count levels for the subs active on that day
	var portions []map[string]int
	for i := 0; i < days; i++ {
		day := first.Unix()
		adoption := make(map[string]int)
		all := 0
		for _, subs := range m {
			for _, sub := range subs {
				if sub.Start < day && sub.End >= day {
					adoption[sub.Level]++
					all++
				}
			}
		}
		adoptionPercent := make(map[string]int)
		for level, subCount := range adoption {
			//avoid division by zero
			if all == 0 {
				adoptionPercent[level] = 0
				continue
			}
			rounded := int(simpleRound(float64(subCount) * 100 / float64(all)))
			adoptionPercent[level] = rounded
		}
		portions = append(portions, adoptionPercent)
		first = first.AddDate(0, 0, 1)
	}
	return portions, nil
}

//GetAdoptionSubs returns an array of objects informing about adoption of different subscription levels, per day.
func GetAdoptionSubs(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	queryValues := r.URL.Query()
	days, _ := strconv.Atoi(queryValues.Get("days"))
	offset, _ := strconv.Atoi(queryValues.Get("offset"))
	portions, err := countSubLevels(ps.ByName("appname"), days, offset)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := json.Marshal(&portions)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Adoption by sub level counted and %d bytes written in response to %s request.\n", written, "GET")
}

func countNewSubs(appname string, days, offset int) ([]int, error) {
	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	first := last.AddDate(0, 0, -days)
	//select count subs with created in between first and last
	//do a loop, count subss for one day
	var counts []int
	for i := 0; i < days; i++ {
		next := first.AddDate(0, 0, 1)
		var c int
		err := db.QueryRow(fmt.Sprintf("SELECT COUNT(*) FROM `%s` WHERE created >= '%d' AND created < '%d'", appname+"Subs", first.Unix(), next.Unix())).Scan(&c)
		if err != nil {
			return nil, err
		}
		counts = append(counts, c)
		first = next
	}
	//first should be last now
	return counts, nil
}

//GetNewSubs returns an array of new subscription counts, per day.
func GetNewSubs(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	queryValues := r.URL.Query()
	days, _ := strconv.Atoi(queryValues.Get("days"))
	offset, _ := strconv.Atoi(queryValues.Get("offset"))
	counts, err := countNewSubs(ps.ByName("appname"), days, offset)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := json.Marshal(&counts)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Adoption by sub level counted and %d bytes written in response to %s request.\n", written, "GET")
}

//select subs active during the period
func subsPeriod(appname string, first, last time.Time) (map[string][]RESTSub, error) {
	subs := make(map[string][]RESTSub)
	rows, err := db.Query(fmt.Sprintf("SELECT username, subID, start, end, created, price, level, name FROM `%s` WHERE end >= '%d' AND start < '%d' ORDER BY username, start", appname+"Subs", first.Unix(), last.Unix()))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var u string
		var s RESTSub
		err := rows.Scan(&u, &s.SubID, &s.Start, &s.End, &s.Created, &s.Price, &s.Level, &s.PlanName)
		if err != nil {
			return nil, err
		}
		subs[u] = append(subs[u], s)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return subs, nil
}

func countConversionLevels(appname string, days, offset, interval int) (map[string]map[string]int, error) {
	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	first := last.AddDate(0, 0, -days)
	extra := last.AddDate(0, 0, interval)
	//select subs active during the period of interest and the conversion interval
	m, err := subsPeriod(appname, first, extra)
	if err != nil {
		return nil, err
	}

	convs := make(map[string]map[string]int)
	totals := make(map[string]int)

	for _, subs := range m {
		for _, sub := range subs {
			if len(convs[sub.Level]) == 0 {
				convs[sub.Level] = make(map[string]int)
			}
			if sub.End > first.Unix() && sub.Start < last.Unix() {
				convEnd := time.Unix(sub.Start, 0).AddDate(0, 0, interval).Unix()
				converted := false
				for _, s := range subs {
					if s.SubID != sub.SubID && s.Start > sub.Start && s.Start < convEnd {
						//converting users are added to the map
						convs[sub.Level][s.Level]++
						totals[sub.Level]++
						//users can convert once per sub, so that the totals add up to 100 percent
						converted = true
						break
					}
				}
				if !converted {
					convs[sub.Level]["none"]++
					totals[sub.Level]++
				}

			}
		}
	}
	convPerCent := make(map[string]map[string]int)
	for from, dests := range convs {
		if len(dests) > 0 {
			convPerCent[from] = make(map[string]int)
		}
		for to, count := range dests {
			//avoiding division by zero
			if totals[from] == 0 {
				convPerCent[from][to] = 0
				continue
			}
			convPerCent[from][to] = count * 100 / totals[from]
		}
	}
	return convPerCent, nil
}

//GetConversions returns a nested object with the conversion percentages for individual subscription levels.
func GetConversions(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	queryValues := r.URL.Query()
	days, _ := strconv.Atoi(queryValues.Get("days"))
	offset, _ := strconv.Atoi(queryValues.Get("offset"))
	interval, _ := strconv.Atoi(queryValues.Get("interval"))
	names, _ := strconv.ParseBool(queryValues.Get("names"))
	//interval defaults to 30 days
	if interval == 0 {
		interval = 30
	}
	var convs map[string]map[string]int
	if names {
		c, err := countConversionNames(ps.ByName("appname"), days, offset, interval)
		if err != nil {
			log.Fatal(err)
		}
		convs = c
	} else {
		c, err := countConversionLevels(ps.ByName("appname"), days, offset, interval)
		if err != nil {
			log.Fatal(err)
		}
		convs = c
	}
	resp, err := json.Marshal(&convs)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Conversion rates counted and %d bytes written in response to %s request.\n", written, "GET")
}

func countConversionNames(appname string, days, offset, interval int) (map[string]map[string]int, error) {
	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	first := last.AddDate(0, 0, -days)
	extra := last.AddDate(0, 0, interval)
	//select subs active during the period of interest and the conversion interval
	m, err := subsPeriod(appname, first, extra)
	if err != nil {
		return nil, err
	}

	convs := make(map[string]map[string]int)
	totals := make(map[string]int)

	for _, subs := range m {
		for _, sub := range subs {
			if len(convs[sub.PlanName]) == 0 {
				convs[sub.PlanName] = make(map[string]int)
			}
			if sub.End > first.Unix() && sub.Start < last.Unix() {
				convEnd := time.Unix(sub.Start, 0).AddDate(0, 0, interval).Unix()
				converted := false
				for _, s := range subs {
					if s.SubID != sub.SubID && s.Start > sub.Start && s.Start < convEnd {
						//converting users are added to the map
						convs[sub.PlanName][s.PlanName]++
						totals[sub.PlanName]++
						//users can convert once per sub, so that the totals add up to 100 percent
						converted = true
						break
					}
				}
				if !converted {
					convs[sub.PlanName]["none"]++
					totals[sub.PlanName]++
				}

			}
		}
	}
	convPerCent := make(map[string]map[string]int)
	for from, dests := range convs {
		if len(dests) > 0 {
			convPerCent[from] = make(map[string]int)
		}
		for to, count := range dests {
			//avoiding division by zero
			if totals[from] == 0 {
				convPerCent[from][to] = 0
				continue
			}
			convPerCent[from][to] = count * 100 / totals[from]
		}
	}
	return convPerCent, nil
}

func countSubNames(appname string, days, offset int) ([]map[string]int, error) {
	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	first := last.AddDate(0, 0, -days)

	m, err := subsPeriod(appname, first, last)
	if err != nil {
		return nil, err
	}

	//for every day, go through the map and count levels for the subs active on that day
	var portions []map[string]int
	for i := 0; i < days; i++ {
		day := first.Unix()
		adoption := make(map[string]int)
		all := 0
		for _, subs := range m {
			for _, sub := range subs {
				if sub.Start < day && sub.End >= day {
					adoption[sub.PlanName]++
					all++
				}
			}
		}
		adoptionPercent := make(map[string]int)
		for name, subCount := range adoption {
			//avoid division by zero
			if all == 0 {
				adoptionPercent[name] = 0
				continue
			}
			rounded := int(simpleRound(float64(subCount) * 100 / float64(all)))
			adoptionPercent[name] = rounded
		}
		portions = append(portions, adoptionPercent)
		first = first.AddDate(0, 0, 1)
	}
	return portions, nil
}

//GetAdoptionPlans returns an array of objects informing about adoption of different plan names, per day. E.g. two plans that provide a gold level subscription for different prices and lengths are distinct in this dataset.
func GetAdoptionPlans(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	queryValues := r.URL.Query()
	days, _ := strconv.Atoi(queryValues.Get("days"))
	offset, _ := strconv.Atoi(queryValues.Get("offset"))
	portions, err := countSubNames(ps.ByName("appname"), days, offset)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := json.Marshal(&portions)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Adoption by plan name counted and %d bytes written in response to %s request.\n", written, "GET")
}

//revenue per user per day
func countRevenue(appname string, days, offset int) ([]float64, error) {
	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	first := last.AddDate(0, 0, -days)

	m, err := subsPeriod(appname, first, last)
	if err != nil {
		return nil, err
	}

	users := len(m)

	var counts []float64
	//naive, iterating over the whole map for each day
	for i := 0; i < days; i++ {
		day := first.Unix()
		var spent float64
		for _, subs := range m {
			//add up the price per month of all subscriptions
			for _, sub := range subs {
				if sub.Start < day && sub.End >= day {
					d := time.Unix(sub.End, 0).Sub(time.Unix(sub.Start, 0))
					duration := math.Ceil(d.Hours() / 24)
					spent += float64(sub.Price) * 30 / duration
				}
			}
		}
		//avoiding division by zero
		if users == 0 {
			counts = append(counts, 0)
			first = first.AddDate(0, 0, 1)
			continue
		}
		rev := spent / float64(users)
		counts = append(counts, rev)
		first = first.AddDate(0, 0, 1)
	}
	return counts, nil
}

//GetRevenue returns an array of average revenue per user, per day.
func GetRevenue(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	queryValues := r.URL.Query()
	days, _ := strconv.Atoi(queryValues.Get("days"))
	offset, _ := strconv.Atoi(queryValues.Get("offset"))
	rev, err := countRevenue(ps.ByName("appname"), days, offset)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := json.Marshal(&rev)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Revenue per user counted and %d bytes written in response to %s request.\n", written, "GET")
}

//revenue per paying user per day, paying users define by spending threshold money per month
//cost of subscriptions is averaged over their length
func countRevenuePaying(appname string, days, offset, threshold int) ([]float64, error) {
	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	first := last.AddDate(0, 0, -days)

	m, err := subsPeriod(appname, first, last)
	if err != nil {
		return nil, err
	}

	th := float64(threshold)

	var counts []float64
	//naive, iterating over the whole map for each day
	for i := 0; i < days; i++ {
		day := first.Unix()
		users := 0
		var spentTotal float64
		for _, subs := range m {
			//add up the price per month of all subscriptions, compare total against threshold
			var spentUser float64
			for _, sub := range subs {
				if sub.Start < day && sub.End >= day {
					d := time.Unix(sub.End, 0).Sub(time.Unix(sub.Start, 0))
					duration := math.Ceil(d.Hours() / 24)
					spentUser += float64(sub.Price) * 30 / duration
				}
			}
			spentTotal += spentUser
			if spentUser >= th {
				users++
			}
		}
		//avoiding division by zero
		if users == 0 {
			counts = append(counts, 0)
			first = first.AddDate(0, 0, 1)
			continue
		}
		rev := spentTotal / float64(users)
		counts = append(counts, rev)
		first = first.AddDate(0, 0, 1)
	}
	return counts, nil
}

//GetRevenuePaying returns an array of average revenue per paying user, per day.
func GetRevenuePaying(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	queryValues := r.URL.Query()
	days, _ := strconv.Atoi(queryValues.Get("days"))
	offset, _ := strconv.Atoi(queryValues.Get("offset"))
	threshold, _ := strconv.Atoi(queryValues.Get("threshold"))
	//threshold defaults to 5
	if threshold == 0 {
		threshold = 5
	}
	rev, err := countRevenuePaying(ps.ByName("appname"), days, offset, threshold)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := json.Marshal(&rev)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Revenue per paying user counted and %d bytes written in response to %s request.\n", written, "GET")
}

func countTag(appname string, days, offset int, tag string) ([]int, error) {
	//calculate the time interval
	now := time.Now()
	last := now.AddDate(0, 0, -offset)
	first := last.AddDate(0, 0, -days)

	//prepare queries, since tables don't change between days
	stTagged, err := db.Prepare(fmt.Sprintf("SELECT COUNT(*) FROM `%s` AS tags,(SELECT username, subID FROM `%s` WHERE end >= ? AND start < ?) AS subs WHERE tags.username = subs.username AND tags.subID = subs.subID AND tags.tag = '%s'", appname+"SubTags", appname+"Subs", tag))
	if err != nil {
		return nil, err
	}
	stAll, err := db.Prepare(fmt.Sprintf("SELECT COUNT(*) FROM `%s` WHERE end >= ? AND start < ?", appname+"Subs"))
	if err != nil {
		return nil, err
	}
	//for every day, select count tags of active subs, select count active subs and divide
	var portions []int
	for i := 0; i < days; i++ {
		next := first.AddDate(0, 0, 1)
		var count, all int
		err = stTagged.QueryRow(first.Unix(), next.Unix()).Scan(&count)
		if err != nil {
			return nil, err
		}
		err = stAll.QueryRow(first.Unix(), next.Unix()).Scan(&all)
		if err != nil {
			return nil, err
		}
		if all == 0 {
			portions = append(portions, 0)
			first = first.AddDate(0, 0, 1)
			continue
		}
		rounded := int(simpleRound(float64(count) * 100 / float64(all)))
		portions = append(portions, rounded)
		first = first.AddDate(0, 0, 1)
	}
	return portions, nil
}

//GetTagPrevalence returns an array of percentages, showing prevalence of specified tag in the currently  active subscriptions.
func GetTagPrevalence(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	queryValues := r.URL.Query()
	days, _ := strconv.Atoi(queryValues.Get("days"))
	offset, _ := strconv.Atoi(queryValues.Get("offset"))
	tag := ps.ByName("tag")
	portions, err := countTag(ps.ByName("appname"), days, offset, tag)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := json.Marshal(&portions)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Prevalence by tag counted and %d bytes written in response to %s request.\n", written, "GET")
}
