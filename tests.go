package main

import (
	"fmt"
	"log"
)

// import (
// 	"fmt"
// 	"log"
// 	"strconv"
// 	"time"
// )

// func testDB() {
// 	//testing database interaction
// 	//check whether Apps table exists
// 	var throwaway string
// 	err := db.QueryRow("SELECT 1 FROM Apps LIMIT 1").Scan(&throwaway)
// 	if err != nil {
// 		fmt.Println("Table Apps doesn't exist.")
// 		fmt.Println("Trying to create table Apps")
// 		err = createApps()
// 		if err != nil {
// 			log.Fatal(err)
// 		}
// 	}
// 	//createApp, initApp
// 	fmt.Println("Trying to insert app.")
// 	originalApp := RESTApp{
// 		AppName: "testApp",
// 		Secret:  "1336secret",
// 		Created: time.Now().Unix(),
// 	}

// 	af, err := insertApp(originalApp)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "insert", "app")
// 		log.Fatal(err)
// 	}
// 	fmt.Printf("App inserted, %d rows affected.\n", af)
// 	err = initApp(originalApp.AppName)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "initialize", "app")
// 		log.Fatal(err)
// 	}
// 	//selectApp
// 	fmt.Println("Trying to select app.")
// 	a, err := selectApp(originalApp.AppName)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "select", "app")
// 		log.Fatal(err)
// 	}
// 	if a != originalApp {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "app")
// 		e := fmt.Errorf("expected %#v got %#v", originalApp, a)
// 		log.Fatal(e)
// 	}
// 	//updateApp
// 	a2 := a
// 	a2.Secret = "gottaChangePass"
// 	fmt.Println("Trying to update app.")
// 	af, err = updateApp(a2)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "update", "app")
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Trying to select app.")
// 	fmt.Printf("Expecting %#v\n", a2)
// 	a3, err := selectApp(a2.AppName)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "select", "app2")
// 		log.Fatal(err)
// 	}
// 	fmt.Printf("Got %#v\n", a3)
// 	if a2.AppName != a3.AppName {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "app")
// 		e := fmt.Errorf("expected %#v got %#v", a2.AppName, a3.AppName)
// 		log.Fatal(e)
// 	}
// 	fmt.Printf("expected %#v got %#v\n", a2.Created, a3.Created)
// 	if a2.Created != a3.Created {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "app")
// 		e := fmt.Errorf("expected %#v got %#v", a2.Created, a3.Created)
// 		log.Fatal(e)
// 	}
// 	if a2.Secret != a3.Secret {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "app")
// 		e := fmt.Errorf("expected %#v got %#v", a2.Secret, a3.Secret)
// 		log.Fatal(e)
// 	}
// 	//this is handled at a higher layer (in the handlers, not in the db interaction functions)
// 	// if a2.Hash == a3.Hash {
// 	// 	fmt.Printf("Error comparing original and db supplied %s.\n", "app")
// 	// 	e := fmt.Errorf("expected hash to change")
// 	// 	log.Fatal(e)
// 	// }
// 	//insertApp once again
// 	a.AppName = "another" + a.AppName
// 	af, err = insertApp(a)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "insert", "app")
// 		log.Fatal(err)
// 	}
// 	err = initApp(a.AppName)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "initialize", "app")
// 		log.Fatal(err)
// 	}
// 	//deleteApp
// 	fmt.Println("Trying to delete app.")
// 	dropAll(a.AppName)
// 	//create, get, modify, delete, some Admins
// 	originalAdmin := RESTAdmin{
// 		"john a",
// 		"1234",
// 		"john@admin.internet",
// 	}
// 	adminTable := originalApp.AppName + "Admins"
// 	//insertAdmin
// 	fmt.Println("Trying to insert admin.")
// 	af, err = insertAdmin(adminTable, originalAdmin)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "insert", "admin")
// 		log.Fatal(err)
// 	}
// 	//selectAdmin
// 	fmt.Println("Trying to select admin.")
// 	ad, err := selectAdmin(adminTable, originalAdmin.Login)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "select", "admin")
// 		log.Fatal(err)
// 	}
// 	if ad != originalAdmin {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "admin")
// 		e := fmt.Errorf("expected %#v got %#v", originalAdmin, ad)
// 		log.Fatal(e)
// 	}
// 	//updateAdmin
// 	fmt.Println("Trying to update admin.")
// 	ad.Password = "newAdminPass4321"
// 	af, err = updateAdmin(adminTable, ad)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "update", "admin")
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Trying to select admin.")
// 	ad2, err := selectAdmin(adminTable, ad.Login)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "select", "admin")
// 		log.Fatal(err)
// 	}
// 	if ad2.Login != ad.Login {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "admin")
// 		e := fmt.Errorf("expected %#v got %#v", ad.Login, ad2.Login)
// 		log.Fatal(e)
// 	}
// 	if ad2.Password != ad.Password {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "admin")
// 		e := fmt.Errorf("expected %#v got %#v", ad.Password, ad2.Password)
// 		log.Fatal(e)
// 	}
// 	if ad2.Email != ad.Email {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "admin")
// 		e := fmt.Errorf("expected %#v got %#v", ad.Email, ad2.Email)
// 		log.Fatal(e)
// 	}
// 	//insertAdmin once again
// 	fmt.Println("Trying to insert admin.")
// 	ad2.Login = "donnie t"
// 	ad2.Password = "uuuge"
// 	ad2.Email = "whitehouse@gov.us"
// 	af, err = insertAdmin(adminTable, ad2)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "insert", "admin")
// 		log.Fatal(err)
// 	}
// 	//deleteAdmin
// 	fmt.Println("Trying to delete admin.")
// 	af, err = deleteAdmin(adminTable, ad.Login)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "delete", "admin")
// 		log.Fatal(err)
// 	}
// 	//create get, modify, delete some Plans
// 	planTable := originalApp.AppName + "Plans"
// 	originalPlan := RESTPlan{
// 		RESTPlanNoArr{
// 			0,
// 			"bronze",
// 			"bronze",
// 			5,
// 			30,
// 			false,
// 			false,
// 		},
// 		nil,
// 	}
// 	//select default plan first with planID 1
// 	fmt.Println("Trying to select plan.")
// 	defaultPlan, err := selectPlan(originalApp.AppName, "0")
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "select", "plan")
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Trying to select plan.")
// 	p, err := selectPlan(originalApp.AppName, "1")
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "select", "plan")
// 		log.Fatal(err)
// 	}
// 	if p.PlanID = 0; defaultPlan.RESTPlanNoArr != p.RESTPlanNoArr {
// 		fmt.Printf("Error comparing db supplied default plan and the initialized first plan %s.\n", "admin")
// 		e := fmt.Errorf("expected %#v got %#v", defaultPlan, p)
// 		log.Fatal(e)
// 	}
// 	//insertPlan
// 	fmt.Println("Trying to select all plans.")
// 	plans, err := selectAllPlansNoTags(planTable)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "select", "all plans")
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Trying to insert plan.")
// 	originalPlan.PlanID = newPlanID(plans)
// 	af, err = insertPlan(originalApp.AppName, originalPlan)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "insert", "plan")
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Trying to select plan.")
// 	p, err = selectPlan(originalApp.AppName, strconv.Itoa(originalPlan.PlanID))
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "select", "plan")
// 		log.Fatal(err)
// 	}
// 	if p.RESTPlanNoArr != originalPlan.RESTPlanNoArr {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "plan")
// 		e := fmt.Errorf("expected %#v got %#v", originalPlan, p)
// 		log.Fatal(e)
// 	}
// 	fmt.Println("Trying to select all plans.")
// 	plans, err = selectAllPlansNoTags(planTable)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "select", "all plans")
// 		log.Fatal(err)
// 	}
// 	pTags := originalPlan
// 	pTags.Name = "bronze with tags"
// 	pTags.Tags = append(pTags.Tags, "discount1", "anniversary1")
// 	pTags.PlanID = newPlanID(plans)
// 	fmt.Println("Trying to insert plan.")
// 	af, err = insertPlan(originalApp.AppName, pTags)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "insert", "plan")
// 		log.Fatal(err)
// 	}
// 	//updatePlan
// 	fmt.Println("Trying to update plan.")
// 	pModifications := RESTPlan{}
// 	pModifications.Price = 100
// 	pModifications.Length = 100
// 	pModifications.Tags = append(pModifications.Tags, "longer", "discount1")
// 	pModifications.PlanID = pTags.PlanID
// 	af, err = updatePlan(originalApp.AppName, pModifications)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "update", "plan")
// 		log.Fatal(err)
// 	}
// 	//select compare updated fields
// 	fmt.Println("Trying to select plan.")
// 	pUpdated, err := selectPlan(originalApp.AppName, strconv.Itoa(pModifications.PlanID))
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "select", "plan")
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Plan selected.")
// 	pTags.Price = pModifications.Price
// 	pTags.Length = pModifications.Length
// 	if pTags.RESTPlanNoArr != pUpdated.RESTPlanNoArr {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "plan")
// 		e := fmt.Errorf("expected %#v got %#v", pTags, pUpdated)
// 		log.Fatal(e)
// 	}
// 	var extraTags []string
// 	for _, tag := range pUpdated.Tags {
// 		if tag != "longer" && tag != "discount1" && tag != "anniversary1" {
// 			extraTags = append(extraTags, tag)
// 		}
// 	}
// 	fmt.Printf("tags gotten: %#v extra tags: %#v\n", pUpdated.Tags, extraTags)
// 	if extraTags != nil {
// 		//there were some extra tags
// 		e := fmt.Errorf("too many tags in modified plan")
// 		log.Fatal(e)
// 	}
// 	//selectAllPlans
// 	fmt.Println("Trying to select all plans.")
// 	plans, err = selectAllPlansNoTags(planTable)
// 	if err != nil {
// 		fmt.Printf("Error when trying to %s %s.\n", "select", "all plans")
// 		log.Fatal(err)
// 	}
// 	for _, plan := range plans {
// 		if plan.Name == "bronze" {
// 			if plan.RESTPlanNoArr != originalPlan.RESTPlanNoArr {
// 				fmt.Printf("Error comparing original and db supplied %s.\n", "plan")
// 				e := fmt.Errorf("expected %#v got %#v", plan, originalPlan)
// 				log.Fatal(e)
// 			}
// 		} else if plan.Name == "bronze with tags" {
// 			if plan.RESTPlanNoArr != pTags.RESTPlanNoArr {
// 				fmt.Printf("Error comparing original and db supplied %s.\n", "plan")
// 				e := fmt.Errorf("expected %#v got %#v", plan, pTags)
// 				log.Fatal(e)
// 			}
// 		} else if plan.Name == "default" {
// 			tmp := RESTPlanNoArr{Level: "default", Name: "default", PlanID: 1}
// 			if plan.RESTPlanNoArr != tmp {
// 				fmt.Printf("Error comparing original and db supplied %s.\n", "plan")
// 				e := fmt.Errorf("expected %#v got %#v", tmp, plan)
// 				log.Fatal(e)
// 			}
// 		} else {
// 			e := fmt.Errorf("unexpected plan supplied by the db")
// 			log.Fatal(e)
// 		}
// 	}
// 	//create, get, modify, replace, delete some Users
// 	originalUser := RESTUser{"jay a", "", "jay@user.com", 1489767880908, false}
// 	userTable := originalApp.AppName + "Users"
// 	//insert user
// 	fmt.Println("Trying to insert user.")
// 	af, err = insertUser(userTable, originalUser)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	//select user
// 	fmt.Println("Trying to select user.")
// 	u, err := selectUser(userTable, originalUser.Username)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	if u != originalUser {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "user")
// 		e := fmt.Errorf("expected %#v got %#v", originalUser, u)
// 		log.Fatal(e)
// 	}
// 	fmt.Println("Trying to update user.")
// 	uModifications := RESTUser{Username: originalUser.Username, Email: "newEmail@email.com", Retired: true}
// 	af, err = updateUser(userTable, uModifications)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Trying to select updated user.")
// 	u, err = selectUser(userTable, uModifications.Username)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	uUpdated := originalUser
// 	uUpdated.Retired = uModifications.Retired
// 	uUpdated.Email = uModifications.Email
// 	if u != uUpdated {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "user")
// 		e := fmt.Errorf("expected %#v got %#v", uUpdated, u)
// 		log.Fatal(e)
// 	}
// 	//replace user
// 	uReplacement := originalUser
// 	uReplacement.Created = 0
// 	fmt.Println("Trying to replace user.")
// 	af, err = replaceUser(userTable, uReplacement)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Trying to select replaced user.")
// 	u, err = selectUser(userTable, uReplacement.Username)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	if u != uReplacement {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "user")
// 		e := fmt.Errorf("expected %#v got %#v", uReplacement, u)
// 		log.Fatal(e)
// 	}
// 	uNew := u
// 	uNew.Username = "ayy 420"
// 	uNew.Retired = false
// 	af, err = insertUser(userTable, uNew)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Trying to delete user.")
// 	af, err = deleteUser(userTable, uNew.Username)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	//create, get, modify, delete some Subs
// 	//create sub
// 	originalSub := newSubFromPlan(originalPlan)
// 	fmt.Printf("Got sub from plan: %#v\n", originalSub)
// 	fmt.Println("Trying to insert a sub")
// 	af, err = insertSub(originalApp.AppName, originalUser.Username, originalSub)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Trying to select a sub.")
// 	s, err := selectSub(originalApp.AppName, originalUser.Username, strconv.Itoa(originalSub.SubID))
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	if s.RESTSubNoArr != originalSub.RESTSubNoArr {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "sub")
// 		e := fmt.Errorf("expected %#v got %#v", originalSub, s)
// 		log.Fatal(e)
// 	}
// 	//modify sub
// 	fmt.Println("Trying to update sub.")
// 	sModifications := RESTSub{RESTSubNoArr{SubID: s.SubID, Level: "lord of the universe"}, nil}
// 	fmt.Println(originalUser.Username)
// 	af, err = updateSub(originalApp.AppName, originalUser.Username, sModifications)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Trying to select updated sub.")
// 	sUpdated, err := selectSub(originalApp.AppName, originalUser.Username, strconv.Itoa(sModifications.SubID))
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	s.Level = sModifications.Level
// 	s.Tags = append(s.Tags, sModifications.Tag)
// 	if s.RESTSubNoArr != sUpdated.RESTSubNoArr {
// 		fmt.Printf("Error comparing original and db supplied %s.\n", "sub")
// 		e := fmt.Errorf("expected %#v got %#v", s, sUpdated)
// 		log.Fatal(e)
// 	}
// 	fmt.Printf("got\t%#v\nexpected\t%#v\n", sUpdated, s)
// 	// if sUpdated.Tags[0] != s.Tags[0] {
// 	// 	fmt.Printf("Error comparing original and db supplied %s.\n", "tag")
// 	// 	e := fmt.Errorf("expected %#v got %#v", s.Tags, sUpdated.Tags)
// 	// 	log.Fatal(e)
// 	// }
// 	sNew := originalSub
// 	subs, err := selectAllSubs(originalApp.AppName, originalUser.Username)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	sNew.SubID = newSubID(subs)
// 	sNew.Level = "bonkers"
// 	sNew.Modified = time.Now().Unix()
// 	af, err = insertSub(originalApp.AppName, originalUser.Username, sNew)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	subs, err = selectAllSubs(originalApp.AppName, originalUser.Username)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Printf("All subs:\n%#v\n%#v\n", subs[0], subs[1])
// 	//timestamps
// 	fmt.Println("Trying to insert timestamps.")
// 	stamp := RESTTimestamp{Time: time.Now().Unix()}
// 	af, err = insertTimestamp(originalApp.AppName+"Timestamps", originalUser.Username, sNew.SubID, stamp)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Println("Trying to select timestamps.")
// 	ts, err := selectTimestamps(originalApp.AppName+"Timestamps", originalUser.Username, strconv.Itoa(sNew.SubID))
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	for i, t := range ts {
// 		fmt.Printf("Timestamp %d:\t%d\n", i, t.Time)
// 	}
// 	//create Subs from modified Plan

// 	// fmt.Println("Trying to delete user.")
// 	// rows, err = deleteUser("testAppUsers", "testUser")
// 	// if err != nil {
// 	// 	log.Fatal(err)
// 	// }
// 	// fmt.Println("Modified rows:", rows)
// 	// fmt.Println("User deleted.")

// 	//cleanup
// 	//dropAll(originalApp.AppName)
// 	//db.Exec(fmt.Sprintf("DROP TABLE Apps"))

// 	fmt.Println("Tests successful.")
// 	return
// }

func dropAll(appname string) {
	//delete everything
	err := dropAppCodes(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s\n", "drop Timestamps")
		log.Fatal(err)
	}
	err = dropAppDiscounts(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s\n", "drop Timestamps")
		log.Fatal(err)
	}
	err = dropAppTimestamps(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s\n", "drop Timestamps")
		log.Fatal(err)
	}
	err = dropAppSubTags(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s\n", "drop SubTags")
		log.Fatal(err)
	}
	err = dropAppSubs(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s\n", "drop Subs")
		log.Fatal(err)
	}
	err = dropAppActivePlanTags(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s\n", "drop PlanTags")
		log.Fatal(err)
	}
	err = dropAppActivePlan(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s\n", "drop Plans")
		log.Fatal(err)
	}
	err = dropAppPlanTags(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s\n", "drop PlanTags")
		log.Fatal(err)
	}
	err = dropAppPlans(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s\n", "drop Plans")
		log.Fatal(err)
	}
	err = dropAppUsers(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s\n", "drop Users")
		log.Fatal(err)
	}
	err = dropAppAdmins(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s\n", "drop Admins")
		log.Fatal(err)
	}
	_, err = deleteApp(appname)
	if err != nil {
		fmt.Printf("Error when trying to %s %s.\n", "delete", "app")
		log.Fatal(err)
	}
	// 	// //cleanup

}
