package main

import (
	"net/mail"
	"unicode"
)

//RESTUser struct is a type of the REST represention of a user
type RESTUser struct {
	Username string `json:"username"`
	Hash     string `json:"hash"`
	Email    string `json:"email"`
	Created  int64  `json:"created,string"`
	Retired  bool   `json:"retired"`
}

//RESTSub is a type of the REST representation of a subscription
type RESTSub struct {
	RESTSubNoArr
	Tags []string `json:"tags,omitempty"`
}

//RESTSubNoArr is used for easier emptiness checking
type RESTSubNoArr struct {
	PlanName string  `json:"planName,omitempty"` //only for unmarshalling planName when creating new subscriptions, never sent anywhere
	SubID    int     `json:"subID,string"`
	Price    float64 `json:"price,string"`
	Start    int64   `json:"start,string"`
	End      int64   `json:"end,string"`
	Level    string  `json:"level"`
	Created  int64   `json:"created,string"`
	Modified int64   `json:"modified,string"`
	Renew    bool    `json:"renew"`
	Tag      string  `json:"tag,omitempty"`
}

//RESTApp is a type of the REST represention of an app
type RESTApp struct {
	AppName  string `json:"appname"`
	Secret   string `json:"secret"`
	Password string `json:"password,omitempty"` //used to specify the password of the admin account on app creation
	Hash     string `json:"hash"`
	Created  int64  `json:"created,string"`
}

//RESTAdmin is a type of the REST representation of an app admin
type RESTAdmin struct {
	Login    string `json:"login"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

//RESTPlan is a type of the REST representation of a subscription plan
type RESTPlan struct {
	RESTPlanNoArr
	Tags []string `json:"tags,omitempty"`
}

//RESTPlanNoArr is used for easier emptiness checking
type RESTPlanNoArr struct {
	Name   string  `json:"name"`
	Level  string  `json:"level"`
	Price  float64 `json:"price,string"`
	Length int     `json:"length,string"`
	Renew  bool    `json:"renew"`
	Active bool    `json:"default,omitempty"`
}

//RESTTimestamp contains information about a single occurence of resource access, ID of the resource accessed is optional
type RESTTimestamp struct {
	Time      int64 `json:"time,string"`
	ContentID int   `json:"contentID,omitempty,string"`
}

//RESTDiscount contains information about discounts available for a specific plan
type RESTDiscount struct {
	PlanName     string  `json:"planName"`
	DiscountName string  `json:"discountName"`
	Percentage   float64 `json:"percentage,string"`
}

//RESTDiscountCode represents a discount code that can be given out
type RESTDiscountCode struct {
	RESTDiscount
	OriginalPrice float64 `json:"originalPrice,string"`
	Code          string  `json:"code"`
}

type RESTTag struct {
	Tag string `json:"tag"`
}

//emptiness checks

func (s *RESTSub) isEmpty() bool {
	if s.Tags == nil && (s.RESTSubNoArr == RESTSubNoArr{}) {
		return true
	}
	return false
}

func (p *RESTPlan) isEmpty() bool {
	if p.Tags == nil && (p.RESTPlanNoArr == RESTPlanNoArr{}) {
		return true
	}
	return false
}

//sanitization methods

func (a *RESTApp) sanitize() bool {
	//AppName has to be ASCII alphanumeric
	if !isAlphanumeric(a.AppName) {
		return false
	}
	//Secret has to be ASCII alphanumeric
	if !isAlphanumeric(a.Secret) {
		return false
	}
	//Hash is not user editable
	a.Hash = ""
	//Created has to be numeric
	//guaranteed by json.Unmarshal
	return true
}

func (u *RESTUser) sanitize() bool {
	//Username has to be ASCII alphanumeric
	if !isAlphanumeric(u.Username) {
		return false
	}
	//Hash is not editable by requests
	u.Hash = ""
	//Email according to RFC 5322, adr.Name is ignored if present
	if u.Email != "" {
		adr, err := mail.ParseAddress(u.Email)
		if err != nil {
			return false
		}
		u.Email = adr.Address
	}
	//Created has to be numeric, guaranteed by json.Unmarshal
	//Retired has to be bool, guaranteed by json.Unmarshal
	return true
}

func (s *RESTSub) sanitize() bool {
	//Level has to be ASCII alphanumeric
	if !isAlphanumeric(s.Level) {
		return false
	}
	//Name has to be ASCII alphanumeric
	if !isAlphanumeric(s.PlanName) {
		return false
	}
	//Tag has to be ASCII alphanumeric
	if !isAlphanumeric(s.Tag) {
		return false
	}
	//Tags have to be ASCII alphanumeric
	for _, tag := range s.Tags {
		if !isAlphanumeric(tag) {
			return false
		}
	}
	//other fields guaranteed by json.Unmarshal
	return true
}

func (ad *RESTAdmin) sanitize() bool {
	//all fields have to be ASCII alphanumeric
	if !isAlphanumeric(ad.Login) {
		return false
	}
	if !isAlphanumeric(ad.Password) {
		return false
	}
	//Email according to RFC 5322, adr.Name is ignored if present
	if ad.Email != "" {
		adr, err := mail.ParseAddress(ad.Email)
		if err != nil {
			return false
		}
		ad.Email = adr.Address
	}
	return true
}

func (p *RESTPlan) sanitize() bool {
	//Name has to be ASCII alphanumeric
	if !isAlphanumeric(p.Name) {
		return false
	}
	//Level has to be ASCII alphanumeric
	if !isAlphanumeric(p.Level) {
		return false
	}
	//Tags have to be ASCII alphanumeric
	for _, tag := range p.Tags {
		if !isAlphanumeric(tag) {
			return false
		}
	}
	return true
}

func (d *RESTDiscount) sanitize() bool {
	if !isAlphanumeric(d.PlanName) {
		return false
	}
	if !isAlphanumeric(d.DiscountName) {
		return false
	}
	return true
}

func (c *RESTDiscountCode) sanitize() bool {
	if !c.RESTDiscount.sanitize() {
		return false
	}
	if !isAlphanumeric(c.Code) {
		return false
	}
	return true
}

func isNumeric(s string) bool {
	for _, r := range s {
		if !unicode.IsDigit(r) {
			return false
		}
	}
	return true
}

func isAlphanumeric(s string) bool {
	for _, r := range s {
		if '0' <= r && r <= '9' {
			continue
		} else if 'a' <= r && r <= 'z' {
			continue
		} else if 'A' <= r && r <= 'Z' {
			continue
		}
		return false
	}
	return true
}

func isCode(s string) bool {
	for _, r := range s {
		if '0' <= r && r <= '9' {
			continue
		} else if 'a' <= r && r <= 'z' {
			continue
		} else if 'A' <= r && r <= 'Z' {
			continue
		} else if r == '-' {
			continue
		}
		return false
	}
	return true
}
