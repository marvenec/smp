Subscription management platform

This is my thesis project. It's a platform for managing subscriptions, it gathers data about users and subscriptions and creates visualizations to make decision making easier.
API documentation is here: https://jsapi.apiary.io/previews/smp2/reference