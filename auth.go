package main

import (
	"encoding/json"
	"net/http"

	"log"

	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/julienschmidt/httprouter"
)

//Claims comment.
type Claims struct {
	Username string
	Password string
	Role     string
	jwt.StandardClaims
}

// auth handles log in requests
func auth(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// inputUsername := ps.ByName("username")
	// inputPassword := ps.ByName("password")
	// inputRole := ps.ByName("role")
	queryValues := r.URL.Query()
	for k, v := range queryValues {
		fmt.Printf("%s:\t%s\n", k, v[0])
	}
	inputUsername := queryValues.Get("username")
	inputPassword := queryValues.Get("password")
	inputRole := queryValues.Get("role")

	fmt.Printf("username:--%s--\n", inputUsername)
	fmt.Printf("password: %s\n", inputPassword)
	fmt.Printf("role: %s\n", inputRole)
	if inputUsername == "" {
		fmt.Fprint(w, "Failure!")
	}
	if inputPassword == "" {
		fmt.Fprint(w, "Failure!")
	}
	if inputRole == "" {
		fmt.Fprint(w, "Failure!")
	}
	var username, password, role string

	err := db.QueryRow("SELECT * FROM Auth WHERE username=?", inputUsername).Scan(&username, &password, &role)
	if err != nil {
		log.Fatal(err)
	}
	if inputPassword == password && inputRole == role {
		//ok
		//put user info into a claim
		claims := Claims{
			Username: username,
			Password: password,
			Role:     role,
		}
		//call func to create JWT & set cookie
		setToken(w, r, claims)
		fmt.Println("JWT created, cookie set.")
		//send response to client
		response := response{
			doLogin{
				Username: username,
				Password: password,
				Role:     role,
				Success:  true,
			},
		}
		b, err := json.Marshal(response)
		if err != nil {
			log.Fatal(err)
		}
		//fmt.Println(response)
		fmt.Println(string(b))
		fmt.Fprint(w, string(b))
		// response := `{doLogin:{"username":"%s","password":"%s","role":"%s"}}`
		// fmt.Fprintf(w, response, username, password, role)
	} else {
		fmt.Fprint(w, "Failure!")
	}
}

type doLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Role     string `json:"role"`
	Success  bool   `json:"success"`
}

type response struct {
	doLogin `json:"doLogin"`
}

func setToken(w http.ResponseWriter, r *http.Request, claims Claims) {
	//expiration times
	expireToken := time.Now().Add(time.Hour * 1).Unix()
	expireCookie := time.Now().Add(time.Hour * 1)

	//set some more claims
	//claims.ExpiresAt = ex
	claims.ExpiresAt = expireToken
	claims.Issuer = "localhost"

	//create new token with claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	//sign with secret, should be something random for real usage?
	//BE AWARE argument of SignedString() needs to be []byte, not interface{} as the function signature suggests
	signed, err := token.SignedString([]byte("secret"))
	if err != nil {
		log.Fatal(err)
	}

	//put into client's cookie
	cookie := http.Cookie{Name: "Auth", Value: signed, Expires: expireCookie, HttpOnly: true, Path: "/protected/"}
	http.SetCookie(w, &cookie)
}

// protector wraps the protected handler and returns NotFound when authentication fails
func protector(protected httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		fmt.Println("protector: triggered")

		//check for cookie existence
		cookie, err := r.Cookie("Auth")
		if err != nil {
			fmt.Println("protector: cookie not found")
			http.NotFound(w, r)
			return
		}

		fmt.Println("protector: cookie found")
		//get token from cookie
		token, err := jwt.ParseWithClaims(cookie.Value, &Claims{}, func(token *jwt.Token) (interface{}, error) {
			// Make sure token's signature wasn't changed

			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Siging method changed")
			}
			//_ = token
			return []byte("secret"), nil
		})
		fmt.Println("protector: cookie parsed")
		//fmt.Println(token) //to get rid of the warning
		if err != nil {
			http.NotFound(w, r)
			return
		}
		if claims, ok := token.Claims.(*Claims); ok && token.Valid {
			fmt.Printf("JWT username: %s\nJWT password: %s\nJWT role: %s\nJWTExpiresAt: %v\n", claims.Username, claims.Password, claims.Role, claims.StandardClaims.ExpiresAt)
			//protected handler doesn't need claims in this case, so just call it with w, r
			//otherwise use withContext
			protected(w, r, ps)
		} else {
			http.NotFound(w, r)
			return
		}
	})
}
