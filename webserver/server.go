package main

import (
	"log"
	"net/http"
)

func main() {
	fs := http.FileServer(http.Dir("/home/users/xp/xpospe03/smp-frontend/dist/"))
	http.Handle("/", fs)
	log.Fatal(http.ListenAndServe(":23457", nil))
}
